<?php

class JembatanController extends Controller {

    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Jembatan('create');
        $this->performAjaxValidation($model);
        if (isset($_POST['Jembatan'])) {
            $model->attributes = $_POST['Jembatan'];
            if ($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $balaiId = Yii::app()->user->getState('balai');
        $c = new CDbCriteria();
        $c->select = 'id,nama';
        if ($balaiId) {
            $c->condition = "id=$balaiId";
        }
        $data['balai'] = CHtml::listData(Balai::model()->findAll($c), 'id', 'nama');
        $this->render('create', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);
        if (isset($_POST['Jembatan'])) {
            $model->attributes = $_POST['Jembatan'];
            if ($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $balaiId = Yii::app()->user->getState('balai');
        $c = new CDbCriteria();
        $c->select = 'id,nama';
        if ($balaiId) {
            $c->condition = "id=$balaiId";
        }
        $data['balai'] = CHtml::listData(Balai::model()->findAll($c), 'id', 'nama');
        $this->render('update', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex() {
        $model = new Jembatan('publik');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Jembatan']))
            $model->attributes = $_GET['Jembatan'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Jembatan('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Jembatan']))
            $model->attributes = $_GET['Jembatan'];

        $balaiId = Yii::app()->user->getState('balai');
        if ($balaiId) {
            $model->dbCriteria->condition = "balai_id=$balaiId AND is_active=1";
        }
        $balaies = Jembatan::model()->findAll(['select'=>'t.balai_id','group'=>'t.balai_id','with'=>['balai'=>['select'=>'nama']]]);
        $data['balai']['']='Tampilkan semua...';
        foreach ($balaies as $val => $v) {
            $data['balai'][$v['balai_id']]=$v['balai']['nama'];
        }
        $this->render('admin', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionActivate($id) {
        if (isset($id) && !empty($id)) {
            Yii::app()->db->createCommand()->update('m_jembatan', ['is_active' => 1], 'id=:id', [':id' => $id]);
        }
    }

    public function actionDeactivate($id) {
        if (isset($id) && !empty($id)) {
            Yii::app()->db->createCommand()->update('m_jembatan', ['is_active' => 0], 'id=:id', [':id' => $id]);
        }
    }

    public function loadModel($id) {
        $model = Jembatan::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'jembatan-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
