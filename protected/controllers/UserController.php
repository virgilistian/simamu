<?php

class UserController extends Controller {

    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';

    /**
     * Displays the generator page.
     */
    public function actionGenerate() {
        $generator = Yii::app()->generator;
        $model = new GenerateForm();
        if (isset($_POST['GenerateForm']) === true) {
            $model->attributes = $_POST['GenerateForm'];
            if ($model->validate() === true) {
                $items = array(
                    'tasks' => array(),
                    'operations' => array(),
                );
                foreach ($model->items as $itemname => $value) {
                    if ((bool) $value === true) {
                        if (strpos($itemname, '*') !== false)
                            $items['tasks'][] = $itemname;
                        else
                            $items['operations'][] = $itemname;
                    }
                }
                $generator->addItems($items['tasks'], CAuthItem::TYPE_TASK);
                $generator->addItems($items['operations'], CAuthItem::TYPE_OPERATION);
                //var_dump($items); die;
                if (($generatedItems = $generator->run()) !== false && $generatedItems !== array()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Item Otorisasi telah berhasil ditambahkan.'));
                    $this->redirect(array('index'));
                } else {
                    Yii::app()->user->setFlash('error', Yii::t('app', 'Semua item otorisasi telah ada atau item tidak berhasil ditambahkan.'));
                    $this->redirect(array('index'));
                }
            }
        }
        $items = $generator->getControllerActions();
        $existingItems = array();
        $this->render('generate', array(
            'model' => $model,
            'items' => $items,
            'existingItems' => $existingItems,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new User;
        $this->performAjaxValidation($model);
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $data['balai'] = CHtml::listData(Balai::model()->findAll(['select' => 'id,nama']), 'id', 'nama');
        $this->render('create', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $oldPass = $model->password;
        $this->performAjaxValidation($model);
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $password = $model->password;
            if (!empty($model->password)) {
                $model->password = User::model()->hashPassword($password);
                $model->password_repeat = $model->password;
            } else {
                $model->password = $oldPass;
                $model->password_repeat = $oldPass;
            }
            if ($model->save()) {
                if ($password && !Yii::app()->userDetail->getIsAdminRole()) {
                    Yii::app()->user->logout();
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Password berhasil diubah, silahkan login kembali.'));
                }
                $redirect = (Yii::app()->userDetail->getIsAdminRole()) ? $this->defaultAction : 'user/' . $id;
                $this->redirect(array($redirect));
            }
        }
        $related['oldcategory'] = [];
        if($model->balai_id){
            $cat = Jembatan::model()->findAll(array('select' => 'id, nama', 'condition' => "balai_id = " . $model->balai_id));
            if ($cat) {
                foreach ($cat as $d) {
                    $related['oldcategory'][] = array('id' => $d->id, 'text' => $d->nama);
            }
            }
        }
        $data['balai'] = CHtml::listData(Balai::model()->findAll(['select' => 'id,nama']), 'id', 'nama');
        $this->render('update', array(
            'model' => $model,
            'data' => $data,
            'related' => $related,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            Yii::app()->user->setFlash('success', Yii::t('app', 'Data berhasil dihapus.'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex() {
        $model = new User('publik');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionGetJembatan($id) {
        $this->layout = false;
        if (Yii::app()->request->isAjaxRequest) {
            $sk = Jembatan::model()->findAll(array('select' => 'id, nama', 'condition' => "balai_id = $id"));
            $data = array();
            foreach ($sk as $d) {
                $data[] = array('id' => $d->id, 'text' => $d->nama);
            }
            echo CJSON::encode($data);
        }
    }

    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
