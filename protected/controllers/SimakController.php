<?php

class SimakController extends Controller
{
    public $layout='//layouts/column2';
    public $defaultAction = 'admin';
    
    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        Yii::import("xupload.models.XUploadForm");
        $files=new XUploadForm;
        $model=new Simak('create');
        $this->performAjaxValidation($model);
        if(isset($_POST['Simak']))
        {
            $model->attributes=$_POST['Simak'];
            if($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $data['tahapan'] = CHtml::listData(Tahapan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['instruksi'] = CHtml::listData(Instruksi::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['proses'] = CHtml::listData(Proses::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['checklist'] = CHtml::listData(Checklist::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['acuan'] = CHtml::listData(Acuan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $this->render('create',array(
            'model'=>$model,
            'data'=>$data,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $this->performAjaxValidation($model);
        if(isset($_POST['Simak']))
        {
            $model->attributes=$_POST['Simak'];
            if($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $data['tahapan'] = CHtml::listData(Tahapan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['instruksi'] = CHtml::listData(Instruksi::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['proses'] = CHtml::listData(Proses::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['checklist'] = CHtml::listData(Checklist::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['acuan'] = CHtml::listData(Acuan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $this->render('update',array(
            'model'=>$model,
            'data'=>$data
        ));
    }

    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $model=new Simak('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Simak']))
            $model->attributes=$_GET['Simak'];

        $data['tahapan'] = CHtml::listData(Tahapan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['instruksi'] = CHtml::listData(Instruksi::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['proses'] = CHtml::listData(Proses::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['checklist'] = CHtml::listData(Checklist::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['acuan'] = CHtml::listData(Acuan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $this->render('admin',array(
            'model'=>$model,
            'data'=>$data,
        ));
    }

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
        $model=new Simak('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Simak']))
            $model->attributes=$_GET['Simak'];

        $data['tahapan'] = CHtml::listData(Tahapan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['instruksi'] = CHtml::listData(Instruksi::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['proses'] = CHtml::listData(Proses::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['checklist'] = CHtml::listData(Checklist::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $data['acuan'] = CHtml::listData(Acuan::model()->findAll(['select'=>'id,uraian']), 'id', 'uraian');
        $this->render('admin',array(
            'model'=>$model,
            'data'=>$data,
        ));
    }

    public function loadModel($id)
    {
        $model=Simak::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='simak-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
