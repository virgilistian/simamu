<?php

class SimakChecklistController extends Controller {

    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';

    public function actionUpdateStatus($id) {
        if (Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest) {
            $status = (isset($_GET['status']) && ($_GET['status'] == 'lengkap')) ? 1 : 0;
            Yii::app()->db->createCommand()->update('t_simak_checklist', ['status' => $status], 'id=:id', [':id' => $id]);
        }
    }

    public function actionCheckAll() {
        $this->layout = false;
        if (Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest) {
            if (!empty($_POST['params'])) {
                foreach ($_POST['params'] as $id) {
                    Yii::app()->db->createCommand()->update('t_simak_checklist', ['status' => 1], 'id=:id', [':id' => $id]);
                }
            }
        }
    }

    public function actionUncheckAll() {
        $this->layout = false;
        if (Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest) {
            if (!empty($_POST['params'])) {
                foreach ($_POST['params'] as $id) {
                    Yii::app()->db->createCommand()->update('t_simak_checklist', ['status' => 0], 'id=:id', [':id' => $id]);
                }
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionCreate() {
        $model = new SimakChecklist('create');
        $this->performAjaxValidation($model);
        if (isset($_POST['SimakChecklist'])) {
            $model->attributes = $_POST['SimakChecklist'];
            if ($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $data['jembatan'] = CHtml::listData(Jembatan::model()->findAll(['select' => 'id,nama']), 'id', 'nama');
        $data['tahapan'] = CHtml::listData(Tahapan::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['instruksi'] = CHtml::listData(Instruksi::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['proses'] = CHtml::listData(Proses::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['checklist'] = CHtml::listData(Checklist::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['acuan'] = CHtml::listData(Acuan::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $this->render('create_admin', array(
            'model' => $model,
            'data' => $data
        ));
    }

    public function actionUpdateOwn($id) {
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);
        if (isset($_POST['SimakChecklist'])) {
            $model->attributes = $_POST['SimakChecklist'];
            if ($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $data['jembatan'] = CHtml::listData(Jembatan::model()->findAll(['select' => 'id,nama']), 'id', 'nama');
        $data['tahapan'] = CHtml::listData(Tahapan::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['instruksi'] = CHtml::listData(Instruksi::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['proses'] = CHtml::listData(Proses::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['checklist'] = CHtml::listData(Checklist::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $data['acuan'] = CHtml::listData(Acuan::model()->findAll(['select' => 'id,uraian']), 'id', 'uraian');
        $this->render('update', array(
            'model' => $model,
            'data' => $data
        ));
    }

    public function actionJembatan($id) {
        $model = new Jembatan('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Jembatan']))
            $model->attributes = $_GET['Jembatan'];
        $model->dbCriteria->condition = "balai_id=$id";
        $userJembatan = Yii::app()->userDetail->getUserJembatan();
        if ($userJembatan) {
            $jembatan = explode(',', $userJembatan);
            $model->dbCriteria->addInCondition('id', $jembatan);
            $model->dbCriteria->compare('is_active', 1, 'AND');
        } else {
            $model->dbCriteria->compare('is_active', 1, 'AND');
        }
        $this->render('jembatan', array(
            'model' => $model,
            'balaiId' => $id,
        ));
    }

    public function actionDetail($id) {
        $model = Jembatan::model()->findByPk($id);
        $this->render('detail', array(
            'model' => $model,
        ));
    }

    public function actionScroll() {

        $criteria = new CDbCriteria;
        $criteria->group = 'tahapan_id,instruksi_id';
        $total = SimakChecklist::model()->count($criteria);
        $pages = new CPagination($total);
        $pages->pageSize = 1;
        $pages->applyLimit($criteria);

        $posts = SimakChecklist::model()->findAll($criteria);

        $this->render('scroll', array(
            'posts' => $posts,
            'pages' => $pages,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionPerencanaan($id) {
        $jembatanId = (isset($id)) ? $id : '';
        $simakChecklist = SimakChecklist::model()->findAllByAttributes(['tahapan_id' => 1, 'jembatan_id' => $jembatanId]);
        $model = new CActiveDataProvider('SimakChecklist', ['criteria' => ['condition' => "tahapan_id=1 AND jembatan_id=$jembatanId"]]);
        if (empty($simakChecklist)) {
            $this->inputSimak($jembatanId);
        }
        $rekap1 = $this->countTahapan($jembatanId, 1);
        $rekap2 = $this->countTahapan($jembatanId, 2);
        $rekap3 = $this->countTahapan($jembatanId, 3);
        $jembatan = Jembatan::model()->findByPk($jembatanId);
        $this->pageTitle = 'Kondisi SMM ' . Yii::app()->setting->class2name($jembatan->nama);
        $this->render('perencanaan', [
            'model' => $model,
            'rekap1' => $rekap1,
            'rekap2' => $rekap2,
            'rekap3' => $rekap3,
            'jembatan' => $jembatan,
            'balaiId' => $jembatan->balai_id,
        ]);
    }

    public function actionPembangunan($id) {
        $jembatanId = (isset($id)) ? $id : '';
        $model = new CActiveDataProvider('SimakChecklist', ['criteria' => ['condition' => "tahapan_id=2 AND jembatan_id=$jembatanId"]]);
        $rekap1 = $this->countTahapan($jembatanId, 1);
        $rekap2 = $this->countTahapan($jembatanId, 2);
        $rekap3 = $this->countTahapan($jembatanId, 3);
        $jembatan = Jembatan::model()->findByPk($jembatanId);
        $this->pageTitle = 'Kondisi SMM ' . Yii::app()->setting->class2name($jembatan->nama);
        $this->render('pembangunan', [
            'model' => $model,
            'rekap1' => $rekap1,
            'rekap2' => $rekap2,
            'rekap3' => $rekap3,
            'jembatan' => $jembatan,
            'balaiId' => $jembatan->balai_id,
        ]);
    }

    public function actionPreservasi($id) {
        $jembatanId = (isset($id)) ? $id : '';
        $model = new CActiveDataProvider('SimakChecklist', ['criteria' => ['condition' => "tahapan_id=3 AND jembatan_id=$jembatanId"]]);
        $rekap1 = $this->countTahapan($jembatanId, 1);
        $rekap2 = $this->countTahapan($jembatanId, 2);
        $rekap3 = $this->countTahapan($jembatanId, 3);
        $jembatan = Jembatan::model()->findByPk($jembatanId);
        $this->pageTitle = 'Kondisi SMM ' . Yii::app()->setting->class2name($jembatan->nama);
        $this->render('preservasi', [
            'model' => $model,
            'rekap1' => $rekap1,
            'rekap2' => $rekap2,
            'rekap3' => $rekap3,
            'jembatan' => $jembatan,
            'balaiId' => $jembatan->balai_id,
        ]);
    }

    public function actionUpdate() {
        if (Yii::app()->request->isAjaxRequest) {
            $id = $_POST['id'];
            $model = $this->loadModel($id);
            $model->status = ($model->status == 1) ? 0 : 1;
            if ($model->save()) {
                $rekap1 = $this->countTahapan($model->jembatan_id, 1);
                $rekap2 = $this->countTahapan($model->jembatan_id, 2);
                $rekap3 = $this->countTahapan($model->jembatan_id, 3);
                if ($model->tahapan_id == 1) {
                    $this->render('perencanaan', ['model' => $model,
                        'rekap1' => $rekap1,
                        'rekap2' => $rekap2,
                        'rekap3' => $rekap3,
                    ]);
                } elseif ($model->tahapan_id == 2) {
                    $this->render('pembangunan', ['model' => $model,
                        'rekap1' => $rekap1,
                        'rekap2' => $rekap2,
                        'rekap3' => $rekap3,
                    ]);
                } else {
                    $this->render('preservasi', ['model' => $model,
                        'rekap1' => $rekap1,
                        'rekap2' => $rekap2,
                        'rekap3' => $rekap3,
                    ]);
                }
                Yii::app()->end();
            }
        }
    }

    public function actionAddKeterangan($id) {
//        Yii::app( )->user->setState( 'files', null ); 
        Yii::import("xupload.models.XUploadForm");
        $files = new XUploadForm;
        $model = $this->loadModel($id);
        $model->scenario = 'addKeterangan';
        $this->performAjaxValidation($model);
        if (isset($_POST['SimakChecklist'])) {
            $model->attributes = $_POST['SimakChecklist'];
            if ($model->save()) {
                Yii::app()->setting->addFiles($id);
                echo CJSON::encode(array('status' => 'success'));
                Yii::app()->end();
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]') {
                    echo $error;
                }
                Yii::app()->end();
            }
        }
        //Grid Berkas
        $data['attachment'] = new CActiveDataProvider('Attachment', array(
            'criteria' => array(
                'condition' => 'simak_checklist_id=' . $id,
                'select' => 'id,name',
            ),
                )
        );
        $balaiId = Jembatan::model()->findByPk($model->jembatan_id)->balai_id;
        $this->render('form_addketerangan', ['model' => $model,
            'files' => $files,
            'data' => $data,
            'balaiId' => $balaiId]);
    }

    protected function countTahapan($id, $tahapan) {
        $count['checklist'] = SimakChecklist::model()->count("jembatan_id=$id AND tahapan_id=$tahapan");
        $count['checked'] = SimakChecklist::model()->count("jembatan_id=$id AND tahapan_id=$tahapan AND status=1");
        $count['percent'] = number_format($count['checked'] / $count['checklist'] * 100, 0);
        return $count;
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex() {
        $model = new SimakChecklist('publik');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['SimakChecklist']))
            $model->attributes = $_GET['SimakChecklist'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new SimakChecklist('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['SimakChecklist']))
            $model->attributes = $_GET['SimakChecklist'];
        $model->dbCriteria->order = 'modified DESC';
        $jembatan = SimakChecklist::model()->findAll(['select' => 't.jembatan_id,jembatan.nama AS nama', 'group' => 't.jembatan_id', 'with' => ['jembatan' => ['select' => 'nama']]]);
        $data['jembatan'][''] = 'Tampilkan semua...';
        foreach ($jembatan as $val => $v) {
            $data['jembatan'][$v['jembatan_id']] = $v['jembatan']['nama'];
        }
        $tahapan = SimakChecklist::model()->findAll(['select' => 't.tahapan_id,tahapan.uraian AS uraian', 'group' => 't.tahapan_id', 'with' => ['tahapan' => ['select' => 'uraian']]]);
        $data['tahapan'][''] = 'Tampilkan semua...';
        foreach ($tahapan as $val => $v) {
            $data['tahapan'][$v['tahapan_id']] = $v['tahapan']['uraian'];
        }
        $instruksi = SimakChecklist::model()->findAll(['select' => 't.instruksi_id,instruksi.uraian AS uraian', 'group' => 't.instruksi_id', 'with' => ['instruksi' => ['select' => 'uraian']]]);
        $data['instruksi'][''] = 'Tampilkan semua...';
        foreach ($instruksi as $val => $v) {
            $data['instruksi'][$v['instruksi_id']] = $v['instruksi']['uraian'];
        }
        $proses = SimakChecklist::model()->findAll(['select' => 't.proses_id,proses.uraian AS uraian', 'group' => 't.proses_id', 'with' => ['proses' => ['select' => 'uraian']]]);
        $data['proses'][''] = 'Tampilkan semua...';
        foreach ($proses as $val => $v) {
            $data['proses'][$v['proses_id']] = $v['proses']['uraian'];
        }
        $checklist = SimakChecklist::model()->findAll(['select' => 't.checklist_id,checklist.uraian AS uraian', 'group' => 't.checklist_id', 'with' => ['checklist' => ['select' => 'uraian']]]);
        $data['checklist'][''] = 'Tampilkan semua...';
        foreach ($checklist as $val => $v) {
            $data['checklist'][$v['checklist_id']] = $v['checklist']['uraian'];
        }
        $this->render('admin', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function loadModel($id) {
        $model = SimakChecklist::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionExport($id) {
        $balaiId = $_GET['balai'];
        $model = SimakChecklist::model()->find(['condition' => 'jembatan_id=:jembatanId', 'params' => [':jembatanId' => $id]]);
        if ($model) {
            if (isset($_POST['SimakChecklist'])) {
                $export = $this->export($id);
                if (!$export)
                    Yii::app()->user->setFlash('error', Yii::t('controller', 'Data not found or could not be exported.'));
            }
            $tahapan = SimakChecklist::model()->findAll(['select' => 't.tahapan_id,tahapan.uraian AS uraian', 'group' => 't.tahapan_id', 'with' => ['tahapan' => ['select' => 'uraian']]]);
            foreach ($tahapan as $val => $v) {
                $data['tahapan'][$v['tahapan_id']] = $v['tahapan']['uraian'];
            }
            $instruksi = SimakChecklist::model()->findAll(['select' => 't.instruksi_id,instruksi.uraian AS uraian', 'group' => 't.instruksi_id', 'with' => ['instruksi' => ['select' => 'uraian']]]);
            $data['instruksi'][''] = 'Tampilkan semua...';
            foreach ($instruksi as $val => $v) {
                $data['instruksi'][$v['instruksi_id']] = $v['instruksi']['uraian'];
            }
            $proses = SimakChecklist::model()->findAll(['select' => 't.proses_id,proses.uraian AS uraian', 'group' => 't.proses_id', 'with' => ['proses' => ['select' => 'uraian']]]);
            $data['proses'][''] = 'Tampilkan semua...';
            foreach ($proses as $val => $v) {
                $data['proses'][$v['proses_id']] = $v['proses']['uraian'];
            }
            $this->render('export', array(
                'model' => $model,
                'data' => $data,
                'balaiId' => $balaiId,
            ));
        } else {
            Yii::app()->user->setFlash('error', Yii::t('controller', "Data checklist jembatan tidak ditemukan."));
            $this->redirect(["simakChecklist/jembatan/$balaiId"]);
        }
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'simak-checklist-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function inputSimak($jembatanId) {
        $simak = Simak::model()->findAll();
        if (!empty($simak)) {
            $data = [];
            $userId = Yii::app()->user->id;
            $date = date('Y-m-d H:m:s', time());
            foreach ($simak as $v) {
                $data[] = [
                    'jembatan_id' => $jembatanId,
                    'tahapan_id' => $v->tahapan_id,
                    'instruksi_id' => $v->instruksi_id,
                    'proses_id' => $v->proses_id,
                    'checklist_id' => $v->checklist_id,
                    'created' => $date,
                    'modified' => $date,
                    'created_by' => $userId,
                    'modified_by' => $userId,
                ];
            }
            $builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand('t_simak_checklist', $data);
            $command->execute();
        }
    }

    protected function export($jembatanId) {
        ini_set('max_execution_time', 360);
        ini_set('memory_limit', '128M');
        Yii::import("ext.yiireport.*");
        Yii::import("application.vendor.phpexcel.*");
        Yii::import('ext.yiiexcel.YiiExcel', true);
        Yii::registerAutoloader(array('YiiExcel', 'autoload'), true);
        PHPExcel_Shared_ZipStreamWrapper::register();
        if (ini_get('mbstring.func_overload') & 2) {
            throw new Exception('Multibyte function overloading in PHP must be disabled for string functions (2).');
        }
        PHPExcel_Shared_String::buildCharacterSets();
        $tahapanId = $_POST['SimakChecklist']['tahapan_id'];
        $command = Yii::app()->db->createCommand();
        $command->select('t.jembatan_id, t.tahapan_id, t.instruksi_id, t.proses_id, t.checklist_id, t.acuan_id, t.status, i.uraian AS instruksi, p.uraian AS proses, c.uraian AS checklist, a.uraian AS acuan, t.keterangan AS keterangan');
        $command->from('t_simak_checklist t');
        $command->leftJoin('m_instruksi i', 'i.id=t.instruksi_id');
        $command->leftJoin('m_proses p', 'p.id=t.proses_id');
        $command->leftJoin('m_checklist c', 'c.id=t.checklist_id');
        $command->leftJoin('m_acuan a', 'a.id=t.acuan_id');
        $command->where("t.jembatan_id=$jembatanId");
        if (isset($tahapanId) && !empty($tahapanId)) {
            $command->andWhere("t.tahapan_id=$tahapanId");
        }
        $rows = $command->queryAll();
        if (!empty($rows)) {
            $head = [
                'jembatan' => Yii::app()->setting->class2name($this->getNamaJembatan($jembatanId)),
                'tahap' => $this->getTahapan($tahapanId),
                'user' => Yii::app()->userDetail->getDisplayName(),
                'timestamp' => strftime("%d/%m/%Y %H:%M", time()),
            ];
            $data = [];
            $i = 1;
            foreach ($rows as $c) {
                $data[] = [
                    'no' => $i,
                    'instruksi' => ($c['instruksi_id']) ? $c['instruksi'] : '',
                    'proses' => ($c['proses_id']) ? $c['proses'] : '',
                    'checklist' => ($c['checklist_id']) ? $c['checklist'] : '',
                    'status' => ($c['status'] == 1) ? 'Sudah' : 'Belum',
                    'acuan' => ($c['acuan_id']) ? $c['acuan'] : '',
                    'keterangan' => ($c['keterangan']) ? $c['keterangan'] : '',
                ];
                $i++;
            }
            $r = new YiiReport(array('template' => 'template_checklist.xlsx'));
            $r->load(array(
                array(
                    'id' => 'head',
                    'data' => $head,
                ),
                array(
                    'id' => 'content',
                    'repeat' => true,
                    'data' => $data,
                ),
                    )
            );
            $datetime = date('Y-m-d H:m:s', time());
            Yii::app()->db->createCommand()->update('m_jembatan', ['last_export_smm' => $datetime], 'id=:id', [':id' => $jembatanId]);
            echo $r->render('excel2007', Yii::app()->controller->id);
            return true;
        }
        return false;
    }

}
