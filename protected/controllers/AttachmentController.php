<?php

class AttachmentController extends Controller {

    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = $this->loadModel($id);
        $this->render('view', ['model' => $model, 'simakChecklistId' => $model->simak_checklist_id]);
    }

    public function actionIndex() {
        $model = new Attachment('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Attachment']))
            $model->attributes = $_GET['Attachment'];

        $model->dbCriteria->order = 't.modified DESC';
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Attachment('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Attachment']))
            $model->attributes = $_GET['Attachment'];

        $model->dbCriteria->order = 't.modified DESC';
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);
            $rootDir = YiiBase::getPathOfAlias('webroot') . $model->path;
            if ($model->filename && file_exists($rootDir . DIRECTORY_SEPARATOR . $model->filename)) {
                unlink($rootDir . DIRECTORY_SEPARATOR . $model->filename);
            }
            $model->delete();
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function loadModel($id) {
        $model = Attachment::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'attachment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionUpload() {
        Yii::import("xupload.models.XUploadForm");
        $path = realpath(Yii::app()->getBasePath() . "/../files/tmp/") . "/";
        $publicPath = Yii::app()->getBaseUrl() . "/files/tmp/";
        header('Vary: Accept');
        if (isset($_SERVER['HTTP_ACCEPT']) && (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
            header('Content-type: application/json');
        } else {
            header('Content-type: text/plain');
        }

        if (isset($_GET["_method"])) {
            if ($_GET["_method"] == "delete") {
                if ($_GET["file"][0] !== '.') {
                    $file = $path . $_GET["file"];
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
                echo json_encode(true);
            }
        } else {
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($model->file !== null) {
                $model->mime_type = $model->file->getType();
                $model->size = $model->file->getSize();
                $model->name = $model->file->getName();
                $filename = md5(uniqid());
                $filename .= "." . $model->file->getExtensionName();
                if ($model->validate()) {
                    $model->file->saveAs($path . $filename);
                    chmod($path . $filename, 0777);

                    if (Yii::app()->user->hasState('files')) {
                        $userFiles = Yii::app()->user->getState('files');
                    } else {
                        $userFiles = array();
                    }
                    $userFiles[] = array(
                        "path" => $path . $filename,
                        "thumb" => $path . "thumbs/pdf.png",
                        "filename" => $filename,
                        'size' => $model->size,
                        'mime' => $model->mime_type,
                        'name' => $model->name,
                    );
                    Yii::app()->user->setState('files', $userFiles);
                    echo json_encode(array(array(
                            "name" => $model->name,
                            "filename" => $filename,
                            "type" => $model->mime_type,
                            "size" => $model->size,
                            "url" => $publicPath . $filename,
                            "thumbnail_url" => $publicPath . "thumbs/pdf.png",
                            "delete_url" => $this->createUrl("upload", array(
                                "_method" => "delete",
                                "file" => $filename
                            )),
                            "delete_type" => "POST"
                    )));
                } else {
                    echo json_encode(array(
                        array("error" => $model->getErrors('file'),
                    )));
                    Yii::log("XUploadAction: " . CVarDumper::dumpAsString($model->getErrors()), CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction"
                    );
                }
            } else {
                throw new CHttpException(500, "Could not upload file");
            }
        }
    }

    public function actionDownload($id) {
        $model = $this->loadModel($id);
        $path = YiiBase::getPathOfAlias('webroot') . $model->path;
        clearstatcache();
        ob_end_clean();
        if (!empty($model->filename) && file_exists($path . $model->filename)) {
            $file = $path . $model->filename;
            header('Pragma: public');
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: no-store, no-cache, must-revalidate');
            header('Cache-Control: pre-check=0, post-check=0, max-age=0');
            header("Pragma: no-cache");
            header("Expires: 0");
            header('Content-Transfer-Encoding: none');
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($file));
            header('Content-Disposition: attachment; filename="' . basename($model->filename) . '"');
            readfile($file);
            Yii::app()->end();
            return;
        } else {
            Yii::app()->user->setFlash('error', Yii::t('app', 'Berkas tidak tersedia.'));
            $this->redirect(['simakChecklist/addKeterangan/' . $model->simak_checklist_id]);
        }
    }

}
