<?php

class BalaiController extends Controller
{
    public $layout='//layouts/column2';
    public $defaultAction = 'admin';
    
    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new Balai('create');
        $this->performAjaxValidation($model);
        if(isset($_POST['Balai']))
        {
            $model->attributes=$_POST['Balai'];
            if($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $this->performAjaxValidation($model);
        if(isset($_POST['Balai']))
        {
            $model->attributes=$_POST['Balai'];
            if($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $model=new Balai('publik');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Balai']))
            $model->attributes=$_GET['Balai'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
        $model=new Balai('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Balai']))
            $model->attributes=$_GET['Balai'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=Balai::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='balai-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
