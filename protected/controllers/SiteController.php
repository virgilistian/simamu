<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionWelcome() {
        $this->layout = 'column1';
        $this->render('welcome');
    }

    public function actionIndex() {
        $model = new Jembatan('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Jembatan']))
            $model->attributes = $_GET['Jembatan'];
        $balaiId = Yii::app()->userDetail->getUserBalai();
        $model->dbCriteria->select = 't.balai_id, COUNT(t.balai_id) AS jumlah';
        if ($balaiId) {
            $model->dbCriteria->condition = "t.balai_id=$balaiId AND t.is_active=1";
            if(Yii::app()->userDetail->getIsPpkRole()) {
                $jembatan = explode(',', Yii::app()->userDetail->getUserJembatan());
                $model->dbCriteria->addInCondition('t.id', $jembatan);
            }
        } else {
            $model->dbCriteria->condition = 'is_active=1';
        }
        $model->dbCriteria->with = ['balai' => ['select' => 'id,nama']];
        $model->dbCriteria->together = true;
        $model->dbCriteria->group = 't.balai_id';
        $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        $loginForm = 0;
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->updateSessionLogin();
                $this->redirect(array('site/welcome'));
            }
            $loginForm = 1;
        }
        $this->render('login', array(
            'model' => $model,
            'loginForm' => $loginForm,
        ));
    }

    protected function updateSessionLogin() {
        $column = ['is_login' => 1, 'ssid' => session_id()];
        $params = [':uid' => Yii::app()->user->id];
        Yii::app()->db->createCommand()->update('user', $column, 'id=:uid', $params);
    }

    protected function updateSessionLogout($id) {
        $column = ['is_login' => 0, 'ssid' => '', 'last_visit' => date("Y-m-d H:i:s")];
        $params = [':uid' => $id];
        Yii::app()->db->createCommand()->update('user', $column, 'id=:uid', $params);
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        $id = Yii::app()->user->id;
        if (!empty($id)) {
            $this->updateSessionLogout($id);
        }
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
