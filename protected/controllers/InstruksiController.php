<?php

class InstruksiController extends Controller
{
    public $layout='//layouts/column2';
    public $defaultAction = 'admin';
    
    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionScroll($id=1)
    {
        $dataProvider = new CActiveDataProvider('Instruksi');
        $this->render('scroll',array(
            'dataProvider'=>$dataProvider,
        ));
    }
    
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new Instruksi('create');
        $this->performAjaxValidation($model);
        if(isset($_POST['Instruksi']))
        {
            $model->attributes=$_POST['Instruksi'];
            if($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $this->performAjaxValidation($model);
        if(isset($_POST['Instruksi']))
        {
            $model->attributes=$_POST['Instruksi'];
            if($model->save())
                $this->redirect(array($this->defaultAction));
        }
        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $model=new Instruksi('publik');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Instruksi']))
            $model->attributes=$_GET['Instruksi'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
        $model=new Instruksi('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Instruksi']))
            $model->attributes=$_GET['Instruksi'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=Instruksi::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='instruksi-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
