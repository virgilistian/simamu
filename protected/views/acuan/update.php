<?php
$this->pageTitle = Yii::t('view', 'Ubah Data').' '.Yii::t('view', 'Acuan');
$this->breadcrumbs=array(
	Yii::t('view', 'Acuan') => array($this->defaultAction),
	Yii::t('view', 'Ubah Data'),
);
?>
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>        </div>
    </div>
</div>