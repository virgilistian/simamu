<?php
$this->pageTitle = Yii::t('view', 'Acuan');
$this->breadcrumbs=array(
	Yii::t('view', 'Acuan') => array('admin'),
);
?>
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'uraian',
		'value' => '($data->uraian) ? $data->uraian : "-"',
	),array(
		'name' => 'keterangan',
		'visible' => false,
		'value' => '($data->keterangan) ? $data->keterangan : "-"',
	),   
        )
    )
); 
?>
<div style="display: none;" tabindex="-1" class="modal fade" id="myModal"></div>