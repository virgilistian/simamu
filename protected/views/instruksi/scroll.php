<?php
$this->breadcrumbs=array(
	'Instruksi'=>array('index'),
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Rincian Data Instruksi');?></h3>
  </div>
    <div class="panel-body"> 
<?php 
$this->widget('booster.widgets.TbListView', array(
       'id' => 'timeline',
       'dataProvider' => $dataProvider,
       'itemView' => '_detail_scroll',
       'template' => '{items} {pager}',
       'pager' => array(
                    'class' => 'ext.infiniteScroll.IasPager', 
                    'rowSelector'=>'.time-label', 
                    'listViewId' => 'timeline', 
                    'header' => '',
                    'loaderText'=>'Loading...',
                    //'options' => array('history' => false, 'triggerPageTreshold' => 2, 'trigger'=>'Load more'),
                  )
            )
       );
?>
    </div>
</div>