<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo Yii::t('view', 'Berkas/Lampiran'); ?></h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div style="height:600px;">
            <?php
            Yii::app()->clientScript->registerCoreScript('jquery');
            $path = str_replace(' ', ' ', $model->path);
            $this->widget('ext.pdfJs.QPdfJs', array(
                'url' => Yii::app()->baseUrl . $path . '/' . $model->filename,
                'options' => Array(
                    // Default sidebar state
                    'sideBarOpen' => false,
                    // ltr = left to right, rtl=right to left
                    'direction' => 'ltr',
                    // Button visible state
                    'buttons' => array(
                        'sidebarToggle' => false,
                        'viewFind' => true,
                        'pageUp' => false,
                        'pageDown' => false,
                        'zoomIn' => true,
                        'zoomOut' => true,
                        'scaleSelect' => false,
                        'presentationMode' => false,
                        'print' => false,
                        'openFile' => false,
                        'download' => true,
                        'viewBookmark' => false,
                    )
                )
            ));
            ?>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <?php
        echo CHtml::link('<i class="fa fa-refresh fa-spin" style="display:none"></i><i class="fa fa-backward"></i> Kembali', CHtml::normalizeUrl(['simakChecklist/addKeterangan', 'id' => $simakChecklistId]), [
            'class' => 'btn btn-default btn-flat',
            'onclick' => 'js:$(".fa-backward").hide();$(".fa-refresh").show();'
        ]);
        ?>
    </div><!-- /.box-footer -->
</div>