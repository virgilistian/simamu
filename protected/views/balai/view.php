<?php
$this->breadcrumbs=array(
	'Balai'=>array('index'),
	$model->nama,
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Rincian Data Balai');?></h3>
  </div>
    <div class="panel-body"> 

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
		'deskripsi',
),
)); ?>
    </div>
</div>