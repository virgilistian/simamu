<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
        'type' => 'horizontal',
	'method'=>'get',
)); ?>

<fieldset>
    <legend><h4><?php echo Yii::t('view', 'Form Pencarian')?></h4></legend>
		<?php echo $form->textFieldGroup($model,'nama',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>50)))); ?>

       <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
            </div>
	</div>

</fieldset>
<?php $this->endWidget(); ?>
