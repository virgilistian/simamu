<?php
$this->pageTitle = Yii::t('view', 'Tambah Data').' '.Yii::t('view', 'Balai');
$this->breadcrumbs=array(
	Yii::t('view', 'Balai') => array($this->defaultAction),
	Yii::t('view', 'Buat Data Baru'),
);
?>
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>        </div>
    </div>
</div>
