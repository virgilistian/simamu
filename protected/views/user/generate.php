<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Manajemen Pengguna') => array('index'),
    Yii::t('app', 'Generate items'),
);
?>
<div id="generator">
    <div class="form">
        <?php $form = $this->beginWidget('CActiveForm'); ?>
        <div class="row-fluid">
            <table class="items generate-item-table table table-condensed table-striped">
                <tbody>
                    <tr class="application-heading-row">
                        <th colspan="3"><?php echo Yii::t('app', 'Application'); ?></th>
                    </tr>
                    <?php
                    $this->renderPartial('_generateItems', array(
                        'model' => $model,
                        'form' => $form,
                        'items' => $items,
                        'existingItems' => $existingItems,
                        'displayModuleHeadingRow' => true,
                        'basePathLength' => strlen(Yii::app()->basePath),
                    ));
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row-fluid">
            <?php
            echo CHtml::link(Yii::t('app', 'Select all'), '#', array(
                'onclick' => "jQuery('.generate-item-table').find(':checkbox').attr('checked', 'checked'); return false;",
                'class' => 'selectAllLink'));
            ?>
            /
            <?php
            echo CHtml::link(Yii::t('app', 'Select none'), '#', array(
                'onclick' => "jQuery('.generate-item-table').find(':checkbox').removeAttr('checked'); return false;",
                'class' => 'selectNoneLink'));
            ?>
        </div>
        <div class="row-fluid">
            <button type="reset" class="btn btn-danger" onclick="history.go(-1);">
                <?php echo Yii::t('app', 'Cancel'); ?>
            </button>
            <button type="submit" class="btn btn-primary">
                <?php echo Yii::t('app', 'Generate'); ?>
            </button>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>