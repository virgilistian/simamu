<?php
$this->breadcrumbs=array(
	'Profil',
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Profil');?></h3>
  </div>
    <div class="panel-body"> 

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
                'username',
		'fullname',
                ['name'=>'is_active','value'=> ($model->is_active == 1) ? 'Aktif' : 'Tidak Aktif'],
                ['name'=>'modified','value'=> 'Tanggal '.$this->getDateTime($model->modified)],
                ['name'=>'last_visit','value'=>  'Tanggal '.$this->getDateTime($model->last_visit)],
                ['name'=>'balai_id','value'=>  $model->balai->nama],
),
)); ?>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
		<?php echo CHtml::link('Ubah Profil', $this->createUrl('user/update',['id'=>$model->id]), ['class'=>'btn btn-primary']);?>
            </div>
	</div>
    </div>
</div>