<?php
$this->pageTitle = Yii::t('view', 'User');
$this->breadcrumbs=array(
	Yii::t('view', 'User') => array('admin'),
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Data User');?></h3>
  </div>
    <div class="panel-body"> 
          
        <div class="btn-group" role="group">
            <?php
            echo CHtml::link(Yii::t('view', 'Tambah Data'), array('create'), array('class' => 'btn btn-default'));
            echo CHtml::link(Yii::t('view','Form Pencarian'), '#', array('class'=>'btn btn-default search-button'));
            ?>
        </div>
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                    'model'=>$model,
                   )); ?>
        </div><!-- search-form -->
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'fullname',
		'value' => '($data->fullname) ? $data->fullname : "-"',
	),array(
		'name' => 'username',
		'value' => '($data->username) ? $data->username : "-"',
	),array(
		'name' => 'is_active',
		'value' => '($data->is_active == 1) ? "Aktif" : "Tidak Aktif"',
	),array(
		'name' => 'is_login',
		'value' => '($data->is_login == 1) ? "Ya" : "Tidak"',
	),array(
		'name' => 'last_visit',
		'value' => '($data->last_visit) ? $data->last_visit : "-"',
	),array(
		'name' => 'balai_id',
		'value' => '($data->balai_id) ? $data->balai->nama : "-"',
	),array(
                'htmlOptions' => array('nowrap'=>'nowrap','class'=>'button-column'),
                'class'=>'booster.widgets.TbButtonColumn',
            ),
        )
    )
); 
?>
    </div>
</div>
<?php
 
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#".Yii::app()->controller->id."-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });
");
?>