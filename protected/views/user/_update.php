<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => Yii::app()->controller->id . '-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form',
    ),
        ));
$disabled = (!Yii::app()->userDetail->getIsAdminRole()) ? true : false;
echo $form->errorSummary($model);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('view', 'Ubah Profil Pengguna') ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $form->textFieldGroup($model, 'fullname', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 100)))); ?>
        <?php echo $form->textFieldGroup($model, 'username', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 100, 'disabled' => true)))); ?>
        <?php $model->password = null; ?>
        <?php echo $form->passwordFieldGroup($model, 'password', array('hint' => Yii::t('view', '*) Biarkan kosong jika password tidak diubah.'), 'wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 100)))); ?>
        <?php echo $form->passwordFieldGroup($model, 'password_repeat', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 100)))); ?>
        <?php echo $form->select2Group($model, 'balai_id', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('data' => $data['balai'], 'htmlOptions' => array('empty' => Yii::t('app', 'Pilih Balai...'), 'class' => 'form-control form-cascade-control', 'maxlength' => 2)))); ?>
    <?php echo $form->select2Group($model, 'jembatan', array('groupOptions' => array('id' => 'Cat'), 'wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('data' => null, 'asDropDownList' => false, 'options' => array('allowClear' => true, 'minimumInputLength' => 0, 'multiple' => true, 'tokenSeparators' => array(', ', ' '), 'data' => 'js: function() { return {results: category}; }'), 'htmlOptions' => array('empty' => Yii::t('app', 'Pilih Jembatan...'), 'placeholder' => Yii::t('app', 'Pilih Jembatan...'), 'class' => 'form-control form-cascade-control', 'maxlength' => 4)))); ?>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => Yii::t('app', 'Simpan'),
                ));
                ?>
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'reset',
                    'context' => 'danger',
                    'label' => Yii::t('app', 'Batal'),
                    'htmlOptions' => array('onclick' => 'history.go(-1);'),
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
<?php
//Balai Related to Jembatan
$jse_code = (!$model->isNewrecord) ? "var category = " . CJSON::encode($related['oldcategory']) . ";" : "var category = [];";
Yii::app()->getClientScript()->registerScript("prg_data", $jse_code, CClientScript::POS_BEGIN);
Yii::app()->getClientScript()->registerScript("relatedBalai", "
var pv;
var catId = $('#" . get_class($model) . "_jembatan').attr('placeholder');
$('#" . get_class($model) . "_balai_id').change(function() {
    var pr = this.value;
    if (pr != pv) {
        pv = this.value;
        $('#Cat').find('.select2-allowclear').removeClass('select2-allowclear');
        $('#Cat').find('.select2-chosen').empty().addClass('select2-default').html(catId);
    }
    if (pr != '') {
        var url = '" . CHtml::normalizeUrl(array('getJembatan')) . "';
        $.getJSON(url, {id: pr}, function(data) { category = data; });
        $('#" . get_class($model) . "_jembatan').focus();
    }
});
$('#" . get_class($model) . "_jembatan').change(function() {
    if (this.value != '') { $('#Cat').find('.select2-chosen').removeClass('select2-default'); }
});
", CClientScript::POS_READY
);
?>
<?php (Yii::app()->booster) ? Yii::app()->booster->registerYiiCss() : null; ?>
