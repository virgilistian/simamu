<?php
$this->pageTitle = Yii::t('view', 'Ubah Data') . ' ' . Yii::t('view', 'User');
$this->breadcrumbs = array(
    Yii::t('view', 'User') => array($this->defaultAction),
    Yii::t('view', 'Ubah Data'),
);
?>
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <?php echo $this->renderPartial('_update', array('model'=>$model,'data'=>$data,'related'=>$related)); ?>        </div>
    </div>
</div>