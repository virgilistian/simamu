<?php
$this->breadcrumbs = array(
    'Jembatan' => array('simakChecklist/jembatan/'.$model->balai_id),
    Yii::app()->setting->class2name($model->nama),
);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('view', 'Rincian Data Jembatan'); ?></h3>
    </div>
    <div class="panel-body"> 
        <?php
        $this->widget('booster.widgets.TbDetailView', array(
            'data' => $model,
            'attributes' => array(
                'no',
                'nama',
                'asal',
                'tujuan',
                'km',
                'tipe',
                'jmlbentang',
                'panjang',
                'lebar',
                'thbuat',
                ['name' => 'modified', 'value' => $this->getDateTime($model->modified) . ' Oleh ' . $model->modifiedBy->fullname]
            ),
        ));
        ?>
    </div>   
    <div class="box-footer">
        <?php
        echo CHtml::link('Kondisi SMM <i class="fa fa-forward"></i>', CHtml::normalizeUrl(['simakChecklist/perencanaan', 'id' => $model->id]), [
            'class' => 'btn btn-default pull-right',
            'onclick' => 'js:$(".content").addClass("loading");'
        ]);
        ?>
    </div>
</div>