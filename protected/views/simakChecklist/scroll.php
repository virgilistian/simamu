<?php
$this->pageTitle = Yii::t('view', 'SimakChecklist');
$this->breadcrumbs=array(
	Yii::t('view', 'SimakChecklist') => array('admin'),
);
?>
	<div id="posts">
	<?php foreach($posts as $post): ?>
	    <div class="post">
	        <p>Autor: <?php echo $post->proses->uraian; ?></p>
	        <p><?php echo $post->checklist->uraian; ?></p>
	    </div>
	<?php endforeach; ?>
	</div>
	<?php $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
	    'contentSelector' => '#posts',
	    'itemSelector' => 'div.post',
	    'loadingText' => 'Loading...',
	    'donetext' => 'This is the end... my only friend, the end',
	    'pages' => $pages,
	)); ?>