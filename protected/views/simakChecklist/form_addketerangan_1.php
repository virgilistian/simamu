<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'simak-checklist-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'type' => 'horizontal',
));?>
<div class="box box-default keterangan">
    <div class="box-header with-border">
      <h3 class="box-title">Tambah Keterangan</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <?php echo $form->hiddenField($model,'id', ['value'=>$model->id])?>
        <?php echo $form->hiddenField($model,'tahapan_id', ['value'=>$model->tahapan_id])?>
      <?php echo $form->textFieldGroup($model,'keterangan',array('wrapperHtmlOptions'=>array('class'=>'col-md-10'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>11)))); ?>
    <div class="form-group">
        <label for="Attachment_files[]" class="col-sm-2 control-label"><?php echo Yii::t('view', 'Berkas/Lampiran'); ?></label>
        <div class="col-sm-10">
        <?php
            $this->widget('xupload.XUpload', array(
                                'url' => Yii::app()->createUrl("attachment/upload"),
                                'model' => $files,
                                'attribute' => 'file',
                                'htmlOptions' => array('id'=>'simak-checklist-form')
            ));
            ?>
        </div>
        <?php echo CHtml::label('Berkas/Lampiran Eksisting', 'Attachment_filename', array('class'=>'col-sm-2 control-label'));?>                           
              <div class="col-md-8 col-sm-9">
                  <?php 
                        $this->widget('booster.widgets.TbGridView',array(
                            'id'=>'attachment-grid',
                            'type' => 'hovered condensed',
                            'dataProvider' => $data['attachment'],
                            'responsiveTable' => true,
                            'hideHeader'=>true,
                            'template'=>'{items}',
                            'columns' => array(
                                array(
                                        'name' => 'filename',
                                        'type' => 'raw',
//                                        'value' => '($data->name) ? CHtml::link($data->name, CHtml::normalizeUrl(["attachment/view", "id"=>$data->id])) : "-"',
                                        'value' => '($data->name) ? CHtml::ajaxLink($data->name, CHtml::normalizeUrl(["attachment/view", "id"=>$data->id]), [
                                                                                                                        "type"=>"POST",
                                                                                                                        "update"=>".content",
                                                                                                                        "beforeSend"=>\'function(){
                                                                                                                                           $(".keterangan").addClass("loading");
                                                                                                                                      }\',
                                                                                                                        "complete"=>\'function(){
                                                                                                                                           $(".keterangan").removeClass("loading");
                                                                                                                                      }\',
                                                                                                                   ]) : "-"',
                                ),array(
                                        'htmlOptions' => array('nowrap'=>'nowrap','class'=>'button-column'),
                                        'class'=>'booster.widgets.TbButtonColumn',
                                        'template'=>'{delete}',
                                        'deleteButtonUrl'=>'CHtml::normalizeUrl(array("attachment/delete", "id"=>$data->id))',
                                    ),
                                ),
                        ));
            ?>
              </div>
      </div>
              <div class="form-group">
               
         </div>
    </div><!-- /.box-body -->
  <div class="box-footer">
      <?php
//        $action = ($model->tahapan_id == 1) ? 'perencanaan' : ($model->tahapan_id == 2) ? 'pembangunan' : 'preservasi';
        if($model->tahapan_id == 1){
            $_action = 'perencanaan';
        } elseif ($model->tahapan_id == 2) {
            $_action = 'pembangunan';
        } else {
            $_action = 'preservasi';
        }
        echo CHtml::ajaxLink('Kembali', CHtml::normalizeUrl(['simakChecklist/'.$_action, 'id'=>$model->tahapan_id]), [
                                         'type'=>'POST',
                                         'update'=>'.content',
                                         'beforeSend'=>'function(){
                                                            $(".keterangan").addClass("loading");
                                                       }',
                                         'complete'=>'function(){
                                                            $(".keterangan").removeClass("loading");
                                                       }',
                                    ], [
                                        'class'=>'btn btn-default btn-flat'
                                    ]);
        
         $this->widget(
                'booster.widgets.TbButton',
                array(
                    'htmlOptions'=>['class'=>'btn btn-info pull-right btn-flat'],
                    'context'=>'primary',
                    'label' => 'Simpan',
                    'buttonType' => 'ajaxSubmit',
                    'url' => CHtml::normalizeUrl(['simakChecklist/addKeterangan','id'=>$model->id]),
                    'ajaxOptions' => array(
                        'dataType' => 'JSON',
                        'type' => 'POST',
                        'data' => 'js:$("#simak-checklist-form").serialize()',
                        'beforeSend'=>'js:function(){$(".keterangan").addClass("loading");}',
                        'success' => 'js:function(data) {
                                if(data.status == "success"){
                                    $(".keterangan").removeClass("loading");
                                } else {
                                    $.each(data, function(key, val) {
                                        $(".simak-checklist-form #"+key+"_em_").text(val);
                                        $(".simak-checklist-form #"+key+"_em_").show();
                                    });
                                }
                                }',
                    )
                )
        );
      ?>
  </div><!-- /.box-footer -->
</div>
<?php $this->endWidget();?>