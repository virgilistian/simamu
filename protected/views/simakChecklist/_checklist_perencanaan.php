<?php if(!empty($data['level1']) && !empty($data['level2']) && !empty($data['level3'])){?>
<ul class="timeline">
    <?php foreach ($data['level1'] as $level1):?>
    <div class="scroll timeline">
    <!-- timeline time label -->
    <li class="time-label">
      <span class="bg-aqua"><?php echo ($level1->instruksi_id) ? $level1->instruksi->uraian : '';?></span>
    </li>
        <!-- /.timeline-label -->
        <?php foreach ($data['level2'] as $level2):?>
            <?php if(($level1->instruksi_id==$level2->instruksi_id)):?>
            <!-- timeline item -->
            <li>
              <i class="fa fa-lock bg-red"></i>
              <div class="timeline-item">
                  <h3 class="timeline-header"><a href="#" data-widget="collapse" ><?php echo ($level2->proses_id) ? $level2->proses->uraian : '';?></a></h3>
                <div class="timeline-body">
                    <div class="box-body">
                      <ul class="todo-list ui-sortable">
                        <?php foreach ($data['level3'] as $level3):?>
                            <?php if(($level2->proses_id==$level3->proses_id && $level1->instruksi_id==$level3->instruksi_id)):?>
                                <li class="check-<?php echo $level3->id?> box collapsed-box">
                                    <!-- todo text -->
                                    <ul class="list-inline">
                                        <li>
                                            <!-- checkbox -->
                                            <?php
                                            $action = Yii::app()->controller->action->id;
                                            $icon = ($level3->status==0) ? '<i class="fa fa-circle-o"></i>' : '<i class="fa fa-check"></i>';
                                            $red = ($level3->status==0) ? '-red' : '';
                                            echo CHtml::ajaxLink($icon, CHtml::normalizeUrl(['simakChecklist/update']), ['type'=>'POST','data'=>['id'=>$level3->id],
                                                  'beforeSend'=>'function(){
                                                              $(".overlay-'.$level3->id.'").show();
                                                          }',
                                                  'complete'=>'function(){
                                                              $(".check-'.$level3->id.'").load("'.CHtml::normalizeUrl(['simakChecklist/'.$action,'id'=>$level3->jembatan_id]).' .check-'.$level3->id.' > *");
                                                              $(".overlay-'.$level3->id.'").hide();
                                                              $(".count-1").load("'.CHtml::normalizeUrl(['simakChecklist/'.$action,'id'=>$level3->jembatan_id]).' .count-1 > *");
                                                          }'
                                                    ],['class'=>'btn btn-social-icon link-'.$level3->id]);
                                            ?>
                                            <span class="text<?php echo $red;?>">
                                                <?php
                                                $char = Yii::app()->setting->limitChar($level3->checklist->uraian,100);
                                                echo ($level3->checklist_id) ? $char : '';
                                                ?>
                                            </span>
                                        </li>
                                        <li class="box-tools pull-right link-black text-sm"> <span class="time"><i class="fa fa-user"></i> Oleh <?php echo $level3->modifiedBy->fullname;?></span>
                                        <?php echo CHtml::ajaxLink('<i class="fa fa-plus"></i>', CHtml::normalizeUrl(['simakChecklist/addKeterangan','id'=>  uniqid()]), ['type'=>'POST','data'=>['id'=>$level3->id],
                                                    'update'=>'.checklist',
                                                    'beforeSend'=>'function(){
                                                              $(".overlay-'.$level3->id.'").show();
                                                          }',
                                                    'complete'=>'function(){
                                                              $(".overlay-'.$level3->id.'").hide();
                                                          }'
                                                    ], ['class'=>'btn btn-box-tool']);?>
                                        </li>
                                        <li class="pull-right overlay-<?php echo $level3->id?>" style="display:none"><i class="fa fa-refresh fa-spin"></i></li>
                                      </ul>
                                  </li>
                            <?php endif;?>
                        <?php endforeach;?>
                      </ul>
                    </div>
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            <?php endif;?>
        <?php endforeach;?>
    </div>
    <?php endforeach;?>
    <li>
      <i class="fa fa-clock-o bg-gray"></i>
    </li>
</ul>
<?php $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
    'contentSelector' => 'ul .timeline',
    'itemSelector' => '.scroll.timeline',
    'loadingText' => 'Loading...',
//    'donetext' => 'This is the end... my only friend, the end',
    'pages' => $pages,
)); ?>
<div class="box-footer">
<?php
echo CHtml::ajaxLink('Tahap Pembangunan <i class="fa fa-forward"></i><i class="fa fa-refresh fa-spin" style="display:none"></i>', CHtml::normalizeUrl(['simakChecklist/pembangunan', 'id'=>$data['jembatanId']]), [
                                 'type'=>'POST',
                                 'update'=>'.content',
                                 'beforeSend'=>'function(){
                                                    $(".fa-forward").hide();
                                                    $(".fa-refresh").show();
                                               }',
                            ], [
                                'class'=>'btn btn-default pull-right',
                            ]);
?>
</div>
<?php } else { ?>
<div class="callout callout-danger">
    <h4>Ups...!</h4>
    <p>Data checklist perencanaan tidak tersedia.</p>
</div>
<?php }?>