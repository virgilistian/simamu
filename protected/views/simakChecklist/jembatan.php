<?php
$this->pageTitle = Yii::t('view', 'Jembatan');
$this->breadcrumbs = array(
    Yii::t('view', 'Jembatan'),
);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('view', 'Data Jembatan Balai ' . Yii::app()->setting->class2name($this->getNamaBalai($balaiId))); ?></h3>
    </div>
    <div class="panel-body"> 

        <div class="btn-group" role="group">
            <?php
            echo CHtml::link(Yii::t('view', 'Tambah Data'), array('jembatan/create'), array('class' => 'btn btn-default'));
            echo CHtml::link(Yii::t('view', 'Form Pencarian'), '#', array('class' => 'btn btn-default search-button'));
            ?>
        </div>
        <div class="search-form" style="display:none">
            <?php
            $this->renderPartial('_search_jembatan', array(
                'model' => $model,
            ));
            ?>
        </div><!-- search-form -->
        <?php
        $this->widget('booster.widgets.TbGridView', array(
            'id' => 'jembatan-grid',
            'type' => 'striped bordered condensed',
            'dataProvider' => $model->search(),
            //'filter' => $model,
            'responsiveTable' => true,
            'columns' => array(
                array(
                    'name' => 'no',
                    'value' => '($data->no) ? $data->no : "-"',
                ), array(
                    'name' => 'nama',
                    'value' => '($data->nama) ? $data->nama : "-"',
                ), array(
                    'name' => 'asal',
                    'value' => '($data->asal) ? $data->asal : "-"',
                ), array(
                    'name' => 'tujuan',
                    'value' => '($data->tujuan) ? $data->tujuan : "-"',
                ), array(
                    'name' => 'km',
                    'value' => '($data->km) ? $data->km : "-"',
                ), array(
                    'name' => 'tipe',
                    'value' => '($data->tipe) ? $data->tipe : "-"',
                ), array(
                    'name' => 'jmlbentang',
                    'value' => '($data->jmlbentang) ? $data->jmlbentang : "-"',
                ), array(
                    'name' => 'panjang',
                    'value' => '($data->panjang) ? $data->panjang : "-"',
                ), array(
                    'name' => 'lebar',
                    'value' => '($data->lebar) ? $data->lebar : "-"',
                ), array(
                    'name' => 'thbuat',
                    'visible' => false,
                    'value' => '($data->thbuat) ? $data->thbuat : "-"',
                ), array(
                    'headerHtmlOptions' => array('width' => 125),
                    'htmlOptions' => array('nowrap' => 'nowrap', 'class' => 'button-column'),
                    'class' => 'booster.widgets.TbButtonColumn',
                    'template' => '{detail}  {print}  {delete}',
                    'buttons' => array(
                        'detail' => array(
                            'label' => "<i class='fa fa-edit'></i>",
                            'options' => array('title' => Yii::t('view', 'Rincian Jembatan'), 'class' => 'btn btn-social-icon btn-dropbox'),
                            'url' => 'CHtml::normalizeUrl(array("simakChecklist/detail", "id"=>$data->id))',
                        ),
                        'print' => array(
                            'label' => "<i class='fa fa-print'></i>",
                            'options' => array('title' => Yii::t('view', 'Ekspor Checklist'), 'class' => 'btn btn-social-icon btn-github'),
                            'url' => 'CHtml::normalizeUrl(array("simakChecklist/export", "id"=>$data->id, "balai"=>$data->balai_id))',
                        ),
                        'delete' => array(
                            'label' => "<i class='fa fa-trash'></i>",
                            'options' => array('title' => Yii::t('view', 'Hapus Jembatan'), 'class' => 'btn btn-social-icon btn-google'),
                            'url' => 'CHtml::normalizeUrl(array("jembatan/delete", "id"=>$data->id))',
                        ),
                    ),
                ),
            )
                )
        );
        ?>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#jembatan-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });
");
?>