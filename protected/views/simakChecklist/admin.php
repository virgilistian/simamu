<?php
$this->pageTitle = Yii::t('view', 'Simak Checklist');
$this->breadcrumbs=array(
	Yii::t('view', 'Simak Checklist') => array('admin'),
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Data Simak Checklist');?></h3>
  </div>
    <div class="panel-body"> 
          
        <div class="btn-group" role="group">
            <?php
            echo CHtml::link(Yii::t('view', 'Tambah Data'), array('create'), array('class' => 'btn btn-default'));
            echo CHtml::link(Yii::t('view','Form Pencarian'), '#', array('class'=>'btn btn-default search-button'));
            ?>
        </div>
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                    'model'=>$model,
                    'data'=>$data,
                   )); ?>
        </div><!-- search-form -->
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'jembatan_id',
		'value' => '($data->jembatan) ? $data->jembatan->nama : "-"',
	),array(
		'name' => 'tahapan_id',
		'value' => '($data->tahapan) ? $data->tahapan->uraian : "-"',
	),array(
		'name' => 'instruksi_id',
		'value' => '($data->instruksi) ? $data->instruksi->uraian : "-"',
	),array(
		'name' => 'proses_id',
		'value' => '($data->proses) ? $data->proses->uraian : "-"',
	),array(
		'name' => 'checklist_id',
		'value' => '($data->checklist) ? $data->checklist->uraian : "-"',
	),array(
		'name' => 'status',
		'visible' => false,
		'value' => '($data->status) ? $data->status : "-"',
	),array(
		'name' => 'keterangan',
		'visible' => false,
		'value' => '($data->keterangan) ? $data->keterangan : "-"',
	),array(
		'name' => 'acuan_id',
		'visible' => false,
		'value' => '($data->acuan_id) ? $data->acuan_id : "-"',
	),array(
                'htmlOptions' => array('nowrap'=>'nowrap','class'=>'button-column'),
                'class'=>'booster.widgets.TbButtonColumn',
                'template'=>'{view} {update} {delete}',
                'buttons'=>array(
                        'update' => array(
                            'url' => 'CHtml::normalizeUrl(array("simakChecklist/updateOwn", "id"=>$data->id))',
                        ),
                ),
            ),
        )
    )
); 
?>
    </div>
</div>
<?php
 
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#".Yii::app()->controller->id."-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });
");
?>