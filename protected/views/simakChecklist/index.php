<?php
$this->pageTitle = Yii::t('view', 'SimakChecklist');
$this->breadcrumbs=array(
	Yii::t('view', 'SimakChecklist') => array('admin'),
);
?>
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'proyek_id',
		'value' => '($data->TProyek) ? $data->TProyek->id : "-"',
	),array(
		'name' => 'jembatan_id',
		'value' => '($data->MJembatan) ? $data->MJembatan->nama : "-"',
	),array(
		'name' => 'tahapan_id',
		'value' => '($data->MTahapan) ? $data->MTahapan->id : "-"',
	),array(
		'name' => 'instruksi_id',
		'value' => '($data->MInstruksi) ? $data->MInstruksi->id : "-"',
	),array(
		'name' => 'proses_id',
		'value' => '($data->MProses) ? $data->MProses->id : "-"',
	),array(
		'name' => 'checklist_id',
		'visible' => false,
		'value' => '($data->MChecklist) ? $data->MChecklist->id : "-"',
	),array(
		'name' => 'status',
		'visible' => false,
		'value' => '($data->status) ? $data->status : "-"',
	),array(
		'name' => 'keterangan',
		'visible' => false,
		'value' => '($data->keterangan) ? $data->keterangan : "-"',
	),   
        )
    )
); 
?>
<div style="display: none;" tabindex="-1" class="modal fade" id="myModal"></div>