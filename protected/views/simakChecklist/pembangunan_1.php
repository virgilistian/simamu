<div class="box box-default color-palette-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-th"></i> <?php echo $this->pageTitle;?></h3>
    </div>
    <div class="box-body">
        <!-- Box Tahapan -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-gray count-1">
                <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Tahap Perencanaan</span>
                  <span class="info-box-number"><?php echo $rekap1['percent'].'%';?></span>
                  <div class="progress">
                    <div style="width: <?php echo $rekap1['percent'].'%';?>" class="progress-bar"></div>
                  </div>
                  <span class="progress-description">
                    <?php echo $rekap1['checked'];?> checklist dari <?php echo $rekap1['checklist'];?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box bg-green count-2">
                <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Tahap Pembangunan</span>
                  <span class="info-box-number"><?php echo $rekap2['percent'].'%';?></span>
                  <div class="progress">
                    <div style="width: <?php echo $rekap2['percent'].'%';?>" class="progress-bar"></div>
                  </div>
                  <span class="progress-description">
                    <?php echo $rekap2['checked'];?> checklist dari <?php echo $rekap2['checklist'];?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-gray count-3">
                <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Tahap Preservasi</span>
                  <span class="info-box-number"><?php echo $rekap3['percent'].'%';?></span>
                  <div class="progress">
                    <div style="width: <?php echo $rekap3['percent'].'%';?>" class="progress-bar"></div>
                  </div>
                  <span class="progress-description">
                    <?php echo $rekap3['checked'];?> checklist dari <?php echo $rekap3['checklist'];?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
        </div>
        <!-- End Box Tahapan -->
    </div><!-- /.row -->
</div><!-- /.box-body -->
<div class="checklist">
<?php if(!empty($data['level1']) && !empty($data['level2']) && !empty($data['level3'])){?>
<ul class="timeline">
    <?php foreach ($data['level1'] as $level1):?>
    <div class="scroll timeline">
    <!-- timeline time label -->
    <li class="time-label">
      <span class="bg-aqua"><?php echo ($level1->instruksi_id) ? $level1->instruksi->uraian : '';?></span>
    </li>
        <!-- /.timeline-label -->
        <?php foreach ($data['level2'] as $level2):?>
            <?php if(($level1->instruksi_id==$level2->instruksi_id)):?>
            <!-- timeline item -->
            <li>
              <i class="fa fa-lock bg-red"></i>
              <div class="timeline-item">
                  <h3 class="timeline-header"><a href="#" data-widget="collapse" ><?php echo ($level2->proses_id) ? $level2->proses->uraian : '';?></a></h3>
                <div class="timeline-body">
                    <div class="box-body">
                      <ul class="todo-list ui-sortable">
                        <?php foreach ($data['level3'] as $level3):?>
                            <?php if(($level2->proses_id==$level3->proses_id && $level1->instruksi_id==$level3->instruksi_id)):?>
                                <li class="check-<?php echo $level3->id?> box collapsed-box">
                                    <!-- todo text -->
                                    <ul class="list-inline">
                                        <li>
                                            <!-- checkbox -->
                                            <?php
                                            $action = Yii::app()->controller->action->id;
                                            $icon = ($level3->status==0) ? '<i class="fa fa-circle-o"></i>' : '<i class="fa fa-check"></i>';
                                            $red = ($level3->status==0) ? '-red' : '';
                                            echo CHtml::ajaxLink($icon, CHtml::normalizeUrl(['simakChecklist/update']), ['type'=>'POST','data'=>['id'=>$level3->id],
                                                  'beforeSend'=>'function(){
                                                              $(".overlay-'.$level3->id.'").show();
                                                          }',
                                                  'complete'=>'function(){
                                                              $(".check-'.$level3->id.'").load("'.CHtml::normalizeUrl(['simakChecklist/'.$action,'id'=>$level3->jembatan_id]).' .check-'.$level3->id.' > *");
                                                              $(".overlay-'.$level3->id.'").hide();
                                                              $(".count-2").load("'.CHtml::normalizeUrl(['simakChecklist/'.$action,'id'=>$level3->jembatan_id]).' .count-2 > *");
                                                          }'
                                                    ],['class'=>'btn btn-social-icon link-'.$level3->id]);
                                            ?>
                                            <span class="text<?php echo $red;?>">
                                                <?php
                                                $char = Yii::app()->setting->limitChar($level3->checklist->uraian,100);
                                                echo ($level3->checklist_id) ? $char : '';
                                                ?>
                                            </span>
                                        </li>
                                        <li class="box-tools pull-right link-black text-sm"> <span class="time"><i class="fa fa-user"></i> Oleh <?php echo $level3->modifiedBy->fullname;?></span>
                                        <?php echo CHtml::link('<i class="fa fa-refresh fa-spin spin-'.$level3->id.'" style="display:none"></i><i class="fa fa-plus plus-'.$level3->id.'"></i>', CHtml::normalizeUrl(['simakChecklist/addKeterangan', 'id'=>$level3->id]), [
                                                        'class'=>'btn btn-box-tool',
                                                        'onclick'=>'js:$(".plus-'.$level3->id.'").hide();$(".spin-'.$level3->id.'").show();'
                                                        ]);
                                            ?>
                                        </li>
                                        <li class="pull-right overlay-<?php echo $level3->id?>" style="display:none"><i class="fa fa-refresh fa-spin"></i></li>
                                      </ul>
                                  </li>
                            <?php endif;?>
                        <?php endforeach;?>
                      </ul>
                    </div>
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            <?php endif;?>
        <?php endforeach;?>
    </div>
    <?php endforeach;?>
    <li>
      <i class="fa fa-clock-o bg-gray"></i>
    </li>
</ul>
<?php $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
    'contentSelector' => 'ul .timeline',
    'itemSelector' => '.scroll.timeline',
    'loadingText' => 'Loading...',
    'donetext' => ' ',
    'pages' => $pages,
)); ?>
<div class="box-footer">
    <?php
    echo CHtml::link('<i class="fa fa-refresh fa-spin spin-perencanaan" style="display:none"></i><i class="fa fa-backward backward-perencanaan"></i> Tahap Perencanaan', CHtml::normalizeUrl(['simakChecklist/perencanaan', 'id'=>$model->jembatan_id]), [
                'class'=>'btn btn-default pull-left',
                'onclick'=>'js:$(".backward-perencanaan").hide();$(".spin-perencanaan").show();'
                ]);
            echo CHtml::link('Tahap Preservasi <i class="fa fa-forward forward-preservasi"></i><i class="fa fa-refresh fa-spin spin-preservasi" style="display:none"></i>', CHtml::normalizeUrl(['simakChecklist/preservasi', 'id'=>$model->jembatan_id]), [
                'class'=>'btn btn-default pull-right',
                'onclick'=>'js:$(".forward-preservasi").hide();$(".spin-preservasi").show();'
                ]);
        ?>
</div>
<?php } else { ?>
<div class="callout callout-danger">
    <h4>Ups...!</h4>
    <p>Data checklist pembangunan tidak tersedia.</p>
</div>
<?php }?>
</div>
<?php (Yii::app()->booster) ? Yii::app()->booster->registerYiiCss() : null; ?>
