<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
        'type' => 'horizontal',
	'method'=>'get',
)); ?>

<fieldset>
    <legend><h4><?php echo Yii::t('view', 'Form Pencarian')?></h4></legend>
                <?php echo $form->select2Group($model,'jembatan_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['jembatan'],'htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                <?php echo $form->select2Group($model,'tahapan_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['tahapan'],'htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                <?php echo $form->select2Group($model,'instruksi_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['instruksi'],'htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                <?php echo $form->select2Group($model,'proses_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['proses'],'htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                <?php echo $form->select2Group($model,'checklist_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['checklist'],'htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                <?php echo $form->dropDownListGroup($model,'status', array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>array(''=>'Tampilkan semua...',0=>'Belum',1=>'Sudah',), 'htmlOptions'=>array('class'=>'form-control')))); ?>

       <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
            </div>
	</div>

</fieldset>
<?php $this->endWidget(); ?>
