<?php
$this->breadcrumbs = array(
    'Jembatan' => ['simakChecklist/jembatan/' . $balaiId],
    Yii::app()->setting->class2name($jembatan->nama) => array('simakChecklist/detail/' . $jembatan->id),
    'Kondisi SMM',
);
?>
<div class="box box-default color-palette-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-th"></i> <?php echo $this->pageTitle; ?></h3>
        &nbsp;       
        <?php
        echo CHtml::link('<i class="fa fa-arrow-circle-left"></i> Data Jembatan Balai ' . Yii::app()->setting->class2name($this->getNamaBalai($balaiId)) . ' <i class="fa fa-refresh fa-spin btn-jembatan-balai" style="display:none"></i>', CHtml::normalizeUrl(['simakChecklist/jembatan', 'id' => $balaiId]), [
            'class' => 'btn btn-xs btn-default btn-flat pull-right bg-aqua',
            'onclick' => 'js:$(".fa-forward").hide();$(".btn-jembatan-balai").show();'
        ]);
        ?>
    </div>
    <div class="box-body">
        <!-- Box Tahapan -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-aqua count-1">
                    <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tahap Perencanaan</span>
                        <span class="info-box-number"><?php echo $rekap1['percent'] . '%'; ?></span>
                        <div class="progress">
                            <div style="width: <?php echo $rekap1['percent'] . '%'; ?>" class="progress-bar"></div>
                        </div>
                        <span class="progress-description">
                            <?php echo $rekap1['checked']; ?> checklist dari <?php echo $rekap1['checklist']; ?>
                        </span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-gray count-2">
                    <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tahap Pembangunan</span>
                        <span class="info-box-number"><?php echo $rekap2['percent'] . '%'; ?></span>
                        <div class="progress">
                            <div style="width: <?php echo $rekap2['percent'] . '%'; ?>" class="progress-bar"></div>
                        </div>
                        <span class="progress-description">
                            <?php echo $rekap2['checked']; ?> checklist dari <?php echo $rekap2['checklist']; ?>
                        </span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-gray count-3">
                    <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tahap Preservasi</span>
                        <span class="info-box-number"><?php echo $rekap3['percent'] . '%'; ?></span>
                        <div class="progress">
                            <div style="width: <?php echo $rekap3['percent'] . '%'; ?>" class="progress-bar"></div>
                        </div>
                        <span class="progress-description">
                            <?php echo $rekap3['checked']; ?> checklist dari <?php echo $rekap3['checklist']; ?>
                        </span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->
        </div>
        <!-- End Box Tahapan -->
    </div><!-- /.row -->
</div><!-- /.box-body -->
<div class="box box-default color-palette-box">
    <!-- .box-body -->
    <div class="box-body table-checklist"><?php
        $this->widget('booster.widgets.TbExtendedGridView', array(
            'id' => Yii::app()->controller->id . '-grid',
            'type' => 'striped hover condensed',
            'dataProvider' => $model,
            'selectableRows' => 10,
            'enableSorting' => false,
            'columns' => array(
                array(
                    'name' => 'instruksi_id',
                    'type' => 'raw',
                    'cssClassExpression' => '($data->status == 1) ? "" : "bg-gray disabled color-palette"',
                    'value' => '($data->instruksi_id) ? $data->instruksi->uraian : "-"',
                ), array(
                    'name' => 'proses_id',
                    'type' => 'raw',
                    'value' => '($data->proses_id) ? $data->proses->uraian : "-"',
                    'cssClassExpression' => '($data->status == 1) ? "" : "bg-gray disabled color-palette"',
                    'htmlOptions' => array('width' => 150, 'style' => 'vertical-align: top'),
                    'headerHtmlOptions' => array('width' => 150),
                ), array(
                    'name' => 'checklist_id',
                    'type' => 'raw',
                    'cssClassExpression' => '($data->status == 1) ? "" : "bg-gray disabled color-palette"',
                    'value' => '($data->checklist_id) ? $data->checklist->uraian : "-"',
                ), array(
                    'name' => 'modified',
                    'type' => 'raw',
                    'cssClassExpression' => '($data->status == 1) ? "" : "bg-gray disabled color-palette"',
                    'value' => '($data->modified) ? $data->getDateTime($data->modified) : "-"',
                    'headerHtmlOptions' => array('width' => 133),
                ), array(
                    'htmlOptions' => array('class' => 'button-column', 'style' => 'vertical-align: top'),
                    'class' => 'booster.widgets.TbButtonColumn',
                    'template' => '{markRead} {markUnread} {comment} {comment-o} {delete}',
                    'buttons' => array(
                        'markRead' => array(
                            'label' => "<i class='glyphicon glyphicon-check'></i>",
                            'visible' => '($data->status == 0) ? true : false',
                            'options' => array('title' => Yii::t('app', 'Sudah Lengkap')),
                            'url' => 'Yii::app()->createUrl("simakChecklist/updateStatus", array("id"=>$data->id,"status"=>"lengkap"))',
                            'click' => 'js:function(e){
                            e.preventDefault(); 
                            var url = $(this).attr("href");
                            var th = this, afterDelete = function(){};
                            $.ajax({
                                type: "POST",
                                url: url,
                                success: function(data) {
                                    jQuery("#' . Yii::app()->controller->id . '-grid").yiiGridView("update");
                                    $(".count-1").load("' . CHtml::normalizeUrl(['simakChecklist/perencanaan', 'id' => $jembatan->id]) . ' .count-1 > *");
                                    afterDelete(th, true, data);
                                },
                                fail: function(XHR) {
                                    alert("' . Yii::t('app', 'Checklist tidak dapat diubah') . '");
                                    return afterDelete(th, false, XHR);
                                }
                            });
                            return false;
                        }',
                        ),
                        'markUnread' => array(
                            'label' => "<i class='fa fa-mail-reply'></i>",
                            'visible' => '($data->status == 1 && (Yii::app()->userDetail->getIsAdminRole() || Yii::app()->userDetail->getIsPpkRole())) ? true : false',
                            'options' => array('title' => Yii::t('app', 'Belum Lengkap')),
                            'url' => 'Yii::app()->createUrl("simakChecklist/updateStatus", array("id"=>$data->id))',
                            'click' => 'js:function(e){
                            e.preventDefault(); 
                            var url = $(this).attr("href");
                            var th = this, afterDelete = function(){};
                            $.ajax({
                                type: "POST",
                                url: url,
                                success: function(data) {
                                    jQuery("#' . Yii::app()->controller->id . '-grid").yiiGridView("update");
                                    $(".count-1").load("' . CHtml::normalizeUrl(['simakChecklist/perencanaan', 'id' => $jembatan->id]) . ' .count-1 > *");
                                    afterDelete(th, true, data);
                                },
                                fail: function(XHR) {
                                    alert("' . Yii::t('app', 'Checklist tidak dapat diubah') . '");
                                    return afterDelete(th, false, XHR);
                                }
                            });
                            return false;
                        }',
                        ),
                        'delete' => [
                            'visible' => '(Yii::app()->userDetail->getIsAdminRole()) ? true : false',
                        ],
                        'comment' => [
                            'visible' => '(!$data->keterangan) ? true : false',
                            'label' => "<i class='fa fa-plus-square'></i>",
                            'options' => array('title' => Yii::t('app', 'Tambah Keterangan')),
                            'url' => 'Yii::app()->createUrl("simakChecklist/addKeterangan", array("id"=>$data->id))',
                        ],
                        'comment-o' => [
                            'visible' => '($data->keterangan) ? true : false',
                            'label' => "<i class='fa  fa-commenting-o'></i>",
                            'options' => array('title' => Yii::t('app', 'Lihat Keterangan')),
                            'url' => 'Yii::app()->createUrl("simakChecklist/addKeterangan", array("id"=>$data->id))',
                        ],
                    ),
                ),
            ),
            'responsiveTable' => true,
            'template' => '{items}{summary}{pager}',
//    'itemsCssClass' => 'table-default-dirjen',
            'htmlOptions' => array('class' => 'grid-view extended-grid-view'),
            'bulkActions' => array(
                'actionButtons' => array(
                    array(
                        'id' => 'check_all',
                        'buttonType' => 'button',
                        'context' => 'success',
                        'encodeLabel' => false,
                        'label' => '<i class="glyphicon glyphicon-check"></i> ' . Yii::t('app', 'Sudah Lengkap'),
                        'click' => 'js:function(values){
                    var th = this, afterDelete = function(){};
                    $(".table-checklist").addClass("loading");
                    $.ajax({
                        type: "POST",
                        url: "' . Yii::app()->createUrl('simakChecklist/checkAll') . '",
                        data: {params: values},
                        success: function(data) {
                            $(".table-checklist").removeClass("loading")
                            jQuery("#' . Yii::app()->controller->id . '-grid").yiiGridView("update");
                            $(".count-1").load("' . CHtml::normalizeUrl(['simakChecklist/perencanaan', 'id' => $jembatan->id]) . ' .count-1 > *");
                            afterDelete(th, true, data);
                        },
                        fail: function(XHR) {
                            $(".table-checklist").removeClass("loading")
                            alert("' . Yii::t('app', 'Checklist tidak dapat diubah') . '");
                            return afterDelete(th, false, XHR);
                        }
                    });
                    return false;
                }',
                    ),
                    array(
                        'id' => 'uncheck_all',
                        'buttonType' => 'button',
                        'context' => 'danger',
                        'htmlOptions' => ['style' => (Yii::app()->userDetail->getIsAdminRole()) ? '' : 'display:none'],
                        'encodeLabel' => false,
                        'label' => '<i class="fa fa-mail-reply"></i> ' . Yii::t('app', 'Belum Lengkap'),
                        'click' => 'js:function(values){
                    var th = this, afterDelete = function(){};
                    $(".table-checklist").addClass("loading");
                    $.ajax({
                        type: "POST",
                        url: "' . Yii::app()->createUrl('simakChecklist/uncheckAll') . '",
                        data: {params: values},
                        success: function(data) {
                            $(".table-checklist").removeClass("loading");
                            jQuery("#' . Yii::app()->controller->id . '-grid").yiiGridView("update");
                            $(".count-1").load("' . CHtml::normalizeUrl(['simakChecklist/perencanaan', 'id' => $jembatan->id]) . ' .count-1 > *");
                            afterDelete(th, true, data);
                        },
                        fail: function(XHR) {
                            $(".table-checklist").removeClass("loading");
                            alert("' . Yii::t('app', 'Checklist tidak dapat diubah') . '");
                            return afterDelete(th, false, XHR);
                        }
                    });
                    return false;
                }',
                    ),
                ),
                'checkBoxColumnConfig' => array(
                    'name' => 'id',
                    'htmlOptions' => array('width' => 5, 'style' => 'vertical-align: top'),
                ),
            ),
        ));
        ?>
    </div><!-- /.box-body -->
</div>
<div class="box-footer">
    <?php
    echo CHtml::link('Tahap Pembangunan <i class="fa fa-forward"></i><i class="fa fa-refresh fa-spin spin-pembangunan-prc" style="display:none"></i>', CHtml::normalizeUrl(['simakChecklist/pembangunan', 'id' => $jembatan->id]), [
        'class' => 'btn btn-default btn-flat pull-right',
        'onclick' => 'js:$(".fa-forward").hide();$(".spin-pembangunan-prc").show();'
    ]);
    ?>
</div>