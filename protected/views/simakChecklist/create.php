<?php
//$this->pageTitle = Yii::t('view', 'Tambah Data').' '.Yii::t('view', 'Simak Checklist');
$this->breadcrumbs=array(
	Yii::t('view', 'Simak Checklist') => array($this->defaultAction),
	Yii::t('view', 'Buat Data Baru'),
);
?>
<div class="box box-default color-palette-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-th"></i> <?php echo $this->pageTitle;?></h3>
    </div>
    <div class="box-body">
          <!-- Box Tahapan -->
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="info-box bg-aqua link-perencanan count-1">
                    <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Tahap Perencanaan</span>
                      <span class="info-box-number"><?php echo $rekap1['percent'].'%';?></span>
                      <div class="progress">
                        <div style="width: <?php echo $rekap1['percent'].'%';?>" class="progress-bar"></div>
                      </div>
                      <span class="progress-description">
                        <?php echo $rekap1['checked'];?> checklist dari <?php echo $rekap1['checklist'];?>
                      </span>
                    </div><!-- /.info-box-content -->
                  </a><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <a href="#" class="info-box bg-green link-pembangunan count-2">
                    <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Tahap Pembangunan</span>
                      <span class="info-box-number"><?php echo $rekap2['percent'].'%';?></span>
                      <div class="progress">
                        <div style="width: <?php echo $rekap2['percent'].'%';?>" class="progress-bar"></div>
                      </div>
                      <span class="progress-description">
                        <?php echo $rekap2['checked'];?> checklist dari <?php echo $rekap2['checklist'];?>
                      </span>
                    </div><!-- /.info-box-content -->
                  </a><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="info-box bg-yellow link-preservasi count-3">
                    <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Tahap Preservasi</span>
                      <span class="info-box-number"><?php echo $rekap3['percent'].'%';?></span>
                      <div class="progress">
                        <div style="width: <?php echo $rekap3['percent'].'%';?>" class="progress-bar"></div>
                      </div>
                      <span class="progress-description">
                        <?php echo $rekap3['checked'];?> checklist dari <?php echo $rekap3['checklist'];?>
                      </span>
                    </div><!-- /.info-box-content -->
                  </a><!-- /.info-box -->
                </div><!-- /.col -->
            </div>
            <!-- End Box Tahapan -->
      </div><!-- /.row -->
    </div><!-- /.box-body -->
    <div class="checklist-perencanaan">
        <?php $this->renderPartial('_perencanaan',array(
                'model'=>$model,
                'data'=>$data,
                'files'=>$files,
                'pages'=>$pages
               )); ?>
    </div><!-- checklist-perencanaan -->
    <div class="checklist-pembangunan" style="display:none">
        <?php $this->renderPartial('_pembangunan',array(
                'model'=>$model,
                'data'=>$data,
//                'files'=>$files,
               )); ?>
    </div><!-- checklist-perencanaan -->
    <div class="checklist-preservasi" style="display:none">
        <?php $this->renderPartial('_preservasi',array(
                'model'=>$model,
                'data'=>$data,
//                'files'=>$files,
               )); ?>
    </div><!-- checklist-perencanaan -->
<?php
Yii::app()->clientScript->registerScript('checklist', "
    $('.link-perencanan').click(function(){
            $('.checklist-perencanaan').show();
            $('.checklist-pembangunan').hide();
            $('.checklist-preservasi').hide();
            return false;
    });
    $('.link-pembangunan').click(function(){
            $('.checklist-pembangunan').show();
            $('.checklist-perencanaan').hide();
            $('.checklist-preservasi').hide();
            return false;
    });
    $('.link-preservasi').click(function(){
            $('.checklist-preservasi').show();
            $('.checklist-perencanaan').hide();
            $('.checklist-pembangunan').hide();
            return false;
    });
");
?>
<?php (Yii::app()->booster) ? Yii::app()->booster->registerYiiCss() : null; ?>
