<?php
$this->breadcrumbs = array(
    'Jembatan' => array('simakChecklist/jembatan/' . $balaiId),
    Yii::app()->setting->class2name($model->jembatan->nama),
);
?>
<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => Yii::app()->controller->id . '-export-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form',
    ),
        ));
?>
<?php echo $form->errorSummary($model); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <?php echo Yii::t('view', 'Eksport Data Checklist Jembatan ') . Yii::app()->setting->class2name($model->jembatan->nama); ?>
            <?php
            $lastExport = $this->getLastExportSmm($model->jembatan_id);
            if (!empty($lastExport)):
                ?>
                &nbsp;<span class="btn-xs pull-right"><i class="fa fa-clock-o"></i> Ekspor Data Terakhir Tanggal <?php echo $this->getDateTime($lastExport); ?></span>
            <?php endif; ?>
        </h3>
    </div>
    <div class="panel-body">
        <?php echo $form->select2Group($model, 'tahapan_id', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('data' => $data['tahapan'], 'htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 2)))); ?>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => 'Eksport',
                ));
                ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
