<?php
$this->breadcrumbs=array(
	Yii::t('view', 'Simak Checklist') => array($this->defaultAction),
	Yii::t('view', 'Buat Data Baru'),
);
?>
<div class="box box-default color-palette-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-th"></i> <?php echo $this->pageTitle;?></h3>
    </div>
    <div class="box-body">
         <?php $this->renderPartial('_box_tahapan',[
                    'rekap1'=>$rekap1,
                    'rekap2'=>$rekap2,
                    'rekap3'=>$rekap3,
                 ]);?>
    </div><!-- /.row -->
</div><!-- /.box-body -->
<div class="checklist">
        <?php $this->renderPartial('_detail_checklist',  [
            'model'=>$model,
            'data'=>$data,
            'files'=>$files,
            'pages'=>$pages,
            'button'=>$button,
           ]);?>
</div>
<?php (Yii::app()->booster) ? Yii::app()->booster->registerYiiCss() : null; ?>
