<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
        'type' => 'horizontal',
	'method'=>'get',
)); ?>

<fieldset>
    <legend><h4><?php echo Yii::t('view', 'Form Pencarian')?></h4></legend>
		<?php echo $form->textFieldGroup($model,'id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>11)))); ?>

		<?php echo $form->textFieldGroup($model,'no',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>100)))); ?>

		<?php echo $form->textFieldGroup($model,'nama',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>255)))); ?>

		<?php echo $form->textFieldGroup($model,'asal',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>255)))); ?>

		<?php echo $form->textFieldGroup($model,'tujuan',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>255)))); ?>

		<?php echo $form->textFieldGroup($model,'km',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control')))); ?>

		<?php echo $form->textFieldGroup($model,'tipe',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>255)))); ?>

		<?php echo $form->textFieldGroup($model,'jmlbentang',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control')))); ?>

		<?php echo $form->textFieldGroup($model,'panjang',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control')))); ?>

		<?php echo $form->textFieldGroup($model,'lebar',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control')))); ?>

		<?php echo $form->datePickerGroup($model,'thbuat',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('options'=>array(),'htmlOptions'=>array()), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click on Month/Year to select a different Month/Year.')); ?>

		<?php echo $form->textFieldGroup($model,'lastupdate',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control')))); ?>

       <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
            </div>
	</div>

</fieldset>
<?php $this->endWidget(); ?>
