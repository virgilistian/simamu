<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => Yii::app()->controller->id . '-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form',
    ),
        ));
?>
<?php echo $form->errorSummary($model); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Data Jembatan</h3>
    </div>
    <div class="panel-body">
        <p class="help-block">Field dengan tanda <span class="required">*</span> tidak boleh kosong.</p>
        <?php echo $form->select2Group($model, 'balai_id', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('data' => $data['balai'], 'htmlOptions' => array('empty' => Yii::t('app', 'Pilih Balai...'), 'class' => 'form-control form-cascade-control', 'maxlength' => 2)))); ?>
        <?php echo $form->textFieldGroup($model, 'no', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 100)))); ?>
        <?php echo $form->textFieldGroup($model, 'nama', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 255)))); ?>
        <?php echo $form->textFieldGroup($model, 'asal', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 255)))); ?>
        <?php echo $form->textFieldGroup($model, 'tujuan', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 255)))); ?>
        <?php echo $form->textFieldGroup($model, 'km', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control')))); ?>
        <?php echo $form->textFieldGroup($model, 'tipe', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control', 'maxlength' => 255)))); ?>
        <?php echo $form->textFieldGroup($model, 'jmlbentang', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control')))); ?>
        <?php echo $form->textFieldGroup($model, 'panjang', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control')))); ?>
        <?php echo $form->textFieldGroup($model, 'lebar', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('htmlOptions' => array('class' => 'form-control form-cascade-control')))); ?>
        <?php echo $form->datePickerGroup($model, 'thbuat', array('wrapperHtmlOptions' => array('class' => 'col-md-8'), 'labelOptions' => array('class' => 'col-md-2'), 'widgetOptions' => array('options' => array('format' => 'yyyy-mm-dd'), 'htmlOptions' => array()), 'prepend' => '<i class="glyphicon glyphicon-calendar"></i>')); ?>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => Yii::t('app', 'Simpan'),
                ));
                ?>
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'reset',
                    'context' => 'danger',
                    'label' => Yii::t('app', 'Batal'),
                    'htmlOptions' => array('onclick' => 'history.go(-1);'),
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
<?php (Yii::app()->booster) ? Yii::app()->booster->registerYiiCss() : null; ?>
