<?php
$this->breadcrumbs=array(
	'Jembatan'=>array('index'),
	$model->nama,
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Rincian Data Jembatan');?></h3>
  </div>
    <div class="panel-body"> 

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'no',
		'nama',
		'asal',
		'tujuan',
		'km',
		'tipe',
		'jmlbentang',
		'panjang',
		'lebar',
		'thbuat',
		'lastupdate',
),
)); ?>
    </div>
</div>