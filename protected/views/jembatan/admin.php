<?php
$this->pageTitle = Yii::t('view', 'Jembatan');
$this->breadcrumbs = array(
    Yii::t('view', 'Jembatan') => array('admin'),
);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('view', 'Data Jembatan'); ?></h3>
    </div>
    <div class="panel-body"> 

        <div class="btn-group" role="group">
            <?php
            echo CHtml::link(Yii::t('view', 'Tambah Data'), array('create'), array('class' => 'btn btn-default'));
            echo CHtml::link(Yii::t('view', 'Form Pencarian'), '#', array('class' => 'btn btn-default search-button'));
            ?>
        </div>
        <div class="search-form" style="display:none">
            <?php
            $this->renderPartial('_search', array(
                'model' => $model,
                'data' => $data,
            ));
            ?>
        </div><!-- search-form -->
        <?php
        $this->widget('booster.widgets.TbGridView', ['id' => Yii::app()->controller->id . '-grid',
            'type' => 'striped bordered condensed',
            'dataProvider' => $model->search(),
            //'filter' => $model,
            'responsiveTable' => true,
            'columns' => [array(
            'name' => 'no',
            'value' => '($data->no) ? $data->no : "-"',
                ), array(
                    'name' => 'nama',
                    'value' => '($data->nama) ? $data->nama : "-"',
                ), array(
                    'name' => 'balai_id',
                    'value' => '($data->balai_id) ? $data->balai->nama : "-"',
                ), array(
                    'name' => 'asal',
                    'value' => '($data->asal) ? $data->asal : "-"',
                ), array(
                    'name' => 'tujuan',
                    'value' => '($data->tujuan) ? $data->tujuan : "-"',
                ), array(
                    'name' => 'km',
                    'value' => '($data->km) ? $data->km : "-"',
                ), array(
                    'name' => 'tipe',
                    'value' => '($data->tipe) ? $data->tipe : "-"',
                ), array(
                    'name' => 'jmlbentang',
                    'value' => '($data->jmlbentang) ? $data->jmlbentang : "-"',
                ), array(
                    'name' => 'panjang',
                    'value' => '($data->panjang) ? $data->panjang : "-"',
                ), array(
                    'name' => 'lebar',
                    'value' => '($data->lebar) ? $data->lebar : "-"',
                ), array(
                    'name' => 'is_active',
                    'type' => 'raw',
                    'value' => '($data->is_active && $data->is_active == 1) ? "<span class=\"btn btn-xs btn-success\"><i class=\"fa fa-fw fa-check\"></i></span>" : "-"',
                ), [
                    'headerHtmlOptions' => array('width' => 125),
                    'htmlOptions' => array('nowrap' => 'nowrap', 'class' => 'button-column'),
                    'class' => 'booster.widgets.TbButtonColumn',
                    'template' => '{activate} {deactivate} {view} {update} {delete}',
                    'buttons' => [
                        'activate' => [
                            'visible' => '($data->is_active == 0 && (Yii::app()->userDetail->getIsAdminRole())) ? true : false',
                            'label' => "<i class='glyphicon glyphicon-share'></i>",
                            'options' => ['title' => Yii::t('view', 'Aktifkan')],
                            'url' => 'CHtml::normalizeUrl(array("jembatan/activate", "id"=>$data->id))',
                            'click' => 'js:function(e){
                                e.preventDefault(); 
                                var url = $(this).attr("href");
                                var th = this, afterDelete = function(){};
                                $.ajax({
                                    type: "GET",
                                    url: url,
                                    success: function(data) {
                                        jQuery("#' . Yii::app()->controller->id . '-grid").yiiGridView("update");
                                        afterDelete(th, true, data);
                                    },
                                    fail: function(XHR) {
                                        alert("' . Yii::t('view', 'Jembatan tidak dapat diaktifkan') . '");
                                        return afterDelete(th, false, XHR);
                                    }
                                });
                                return false;
                            }',
                        ],
                        'deactivate' => [
                            'visible' => '($data->is_active == 1 && (Yii::app()->userDetail->getIsAdminRole())) ? true : false',
                            'label' => "<i class='glyphicon glyphicon-ban-circle'></i>",
                            'options' => ['title' => Yii::t('view', 'Non Aktifkan')],
                            'url' => 'CHtml::normalizeUrl(array("jembatan/deactivate", "id"=>$data->id))',
                            'click' => 'js:function(e){
                                e.preventDefault(); 
                                var url = $(this).attr("href");
                                var th = this, afterDelete = function(){};
                                $.ajax({
                                    type: "GET",
                                    url: url,
                                    success: function(data) {
                                        jQuery("#' . Yii::app()->controller->id . '-grid").yiiGridView("update");
                                        afterDelete(th, true, data);
                                    },
                                    fail: function(XHR) {
                                        alert("' . Yii::t('view', 'Jembatan tidak dapat dinonaktifkan') . '");
                                        return afterDelete(th, false, XHR);
                                    }
                                });
                                return false;
                            }',
                        ],
                    ]
                ],
            ]
                ]
        );
        ?>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#" . Yii::app()->controller->id . "-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });
");
?>