<?php
$this->pageTitle = Yii::t('view', 'Jembatan');
$this->breadcrumbs=array(
	Yii::t('view', 'Jembatan') => array('admin'),
);
?>
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'no',
		'value' => '($data->no) ? $data->no : "-"',
	),array(
		'name' => 'nama',
		'value' => '($data->nama) ? $data->nama : "-"',
	),array(
		'name' => 'asal',
		'visible' => false,
		'value' => '($data->asal) ? $data->asal : "-"',
	),array(
		'name' => 'tujuan',
		'visible' => false,
		'value' => '($data->tujuan) ? $data->tujuan : "-"',
	),array(
		'name' => 'km',
		'visible' => false,
		'value' => '($data->km) ? $data->km : "-"',
	),array(
		'name' => 'tipe',
		'visible' => false,
		'value' => '($data->tipe) ? $data->tipe : "-"',
	),array(
		'name' => 'jmlbentang',
		'visible' => false,
		'value' => '($data->jmlbentang) ? $data->jmlbentang : "-"',
	),array(
		'name' => 'panjang',
		'visible' => false,
		'value' => '($data->panjang) ? $data->panjang : "-"',
	),array(
		'name' => 'lebar',
		'visible' => false,
		'value' => '($data->lebar) ? $data->lebar : "-"',
	),array(
		'name' => 'thbuat',
		'visible' => false,
		'value' => '($data->thbuat) ? $data->thbuat : "-"',
	),array(
		'name' => 'lastupdate',
		'visible' => false,
		'value' => '($data->lastupdate) ? $data->lastupdate : "-"',
	),   
        )
    )
); 
?>
<div style="display: none;" tabindex="-1" class="modal fade" id="myModal"></div>