<?php
$this->breadcrumbs=array(
	'Proses'=>array('index'),
	$model->id,
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Rincian Data Proses');?></h3>
  </div>
    <div class="panel-body"> 

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'uraian',
		'sat_waktu',
		'waktu',
),
)); ?>
    </div>
</div>