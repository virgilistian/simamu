<?php
$this->pageTitle = Yii::t('view', 'Proses');
$this->breadcrumbs=array(
	Yii::t('view', 'Proses') => array('admin'),
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Data Proses');?></h3>
  </div>
    <div class="panel-body"> 
          
        <div class="btn-group" role="group">
            <?php
            echo CHtml::link(Yii::t('view', 'Tambah Data'), array('create'), array('class' => 'btn btn-default'));
            echo CHtml::link(Yii::t('view','Form Pencarian'), '#', array('class'=>'btn btn-default search-button'));
            ?>
        </div>
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                    'model'=>$model,
                   )); ?>
        </div><!-- search-form -->
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'uraian',
		'value' => '($data->uraian) ? $data->uraian : "-"',
	),array(
		'name' => 'sat_waktu',
		'visible' => false,
		'value' => '($data->sat_waktu) ? $data->sat_waktu : "-"',
	),array(
		'name' => 'waktu',
		'visible' => false,
		'value' => '($data->waktu) ? $data->waktu : "-"',
	),            array(
                'htmlOptions' => array('nowrap'=>'nowrap','class'=>'button-column'),
                'class'=>'booster.widgets.TbButtonColumn'
            ),
        )
    )
); 
?>
    </div>
</div>
<?php
 
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#".Yii::app()->controller->id."-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });
");
?>