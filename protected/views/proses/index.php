<?php
$this->pageTitle = Yii::t('view', 'Proses');
$this->breadcrumbs=array(
	Yii::t('view', 'Proses') => array('admin'),
);
?>
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'uraian',
		'value' => '($data->uraian) ? $data->uraian : "-"',
	),array(
		'name' => 'sat_waktu',
		'visible' => false,
		'value' => '($data->sat_waktu) ? $data->sat_waktu : "-"',
	),array(
		'name' => 'waktu',
		'visible' => false,
		'value' => '($data->waktu) ? $data->waktu : "-"',
	),   
        )
    )
); 
?>
<div style="display: none;" tabindex="-1" class="modal fade" id="myModal"></div>