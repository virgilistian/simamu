<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
$displayTextLogin = ($loginForm == 1) ? 'style="display:none"' : '';
$displayLoginForm = ($loginForm == 1) ? '' : 'style="display:none"';
?> 
<div class="row text-login"<?php echo $displayTextLogin;?>>
   <div class="col-md-12">
       <div class="col-md-12">
        <div class="alert alert-info alert-dismissable text-center">
            <h1><strong>SISTEM MANAJEMEN MUTU JEMBATAN</strong></h1>
        </div>
       </div>
       <div class="col-lg-2 pull-right">
           <?php echo CHtml::link('Sign in', '#', ['class'=>'btn btn-primary btn-block btn-flat button-login'])?>
       </div>
    </div>
</div>
<div class="login-box">
<!--      <div class="login-logo">
        <a href="#">Sistem Informasi Manajemen Mutu</a>
      </div> /.login-logo -->
<div id="login"<?php echo $displayLoginForm;?>>
        <p class="login-box-msg"></p>
                <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
                          'id'=>'login-form',
                      'enableAjaxValidation' => false,
                      //'type' => 'inline',
                  )); ?>
          <div class="form-group has-feedback">
            <?php echo $form->textFieldGroup($model,'username',array('label'=>false),array('class'=>'form-control','placeholder'=>'Username')); ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <?php echo $form->passwordFieldGroup($model,'password',array('label'=>false),array('class'=>'form-control','placeholder'=>'Password')); ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
       <?php $this->endWidget(); ?>
        <!--<span class="pull-left"><b>Versi</b> 1.0.1</span><br>-->
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box --> 
    <?php
    Yii::app()->clientScript->registerScript('search', "
        $('.button-login').click(function(){
                $('.text-login').hide();
                $('#login').toggle();
                return false;
        });
    ");
    ?>