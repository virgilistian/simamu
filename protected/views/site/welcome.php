<?php
$this->pageTitle = Yii::t('view', 'Balai');
?>
<div class="row">
    <div class="col-md-12 box-welcome">
        <div class="alert alert-info alert-dismissable">
          <h4>Anda masuk sebagai user <?php echo Yii::app()->userDetail->getRoleName();?></h4>
          <p>Silahkan klik "<strong><?php echo CHtml::link('Lanjutkan', Yii::app()->createUrl('/'));?></strong>" untuk masuk ke halaman administrasi.</p>
        </div>
    </div>
    <div class="col-md-12 box-image">
        <div class="col-md-3">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-1.png" class="img-responsive">
        </div>
        <div class="col-md-3">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-2.png" class="img-responsive">
        </div>
        <div class="col-md-3">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-3.png" class="img-responsive">
        </div>
        <div class="col-md-3">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-4.png" class="img-responsive">
        </div>
    </div>
</div>