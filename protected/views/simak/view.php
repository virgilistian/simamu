<?php
$this->breadcrumbs=array(
	'Simak'=>array('index'),
	$model->id,
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo Yii::t('view','Rincian Data Simak');?></h3>
  </div>
    <div class="panel-body"> 

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'tahapan_id',
		'instruksi_id',
		'proses_id',
		'checklist_id',
		'acuan_id',
),
)); ?>
    </div>
</div>