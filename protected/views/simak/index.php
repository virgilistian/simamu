<?php
$this->pageTitle = Yii::t('view', 'Simak');
$this->breadcrumbs=array(
	Yii::t('view', 'Simak') => array('admin'),
);
?>
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'columns' => array(
            array(
		'name' => 'tahapan_id',
		'value' => '($data->MTahapan) ? $data->MTahapan->id : "-"',
	),array(
		'name' => 'instruksi_id',
		'value' => '($data->MInstruksi) ? $data->MInstruksi->id : "-"',
	),array(
		'name' => 'proses_id',
		'value' => '($data->MProses) ? $data->MProses->id : "-"',
	),array(
		'name' => 'checklist_id',
		'value' => '($data->MChecklist) ? $data->MChecklist->id : "-"',
	),array(
		'name' => 'acuan_id',
		'visible' => false,
		'value' => '($data->acuan_id) ? $data->acuan_id : "-"',
	),   
        )
    )
); 
?>
<div style="display: none;" tabindex="-1" class="modal fade" id="myModal"></div>