<?php
$this->pageTitle = Yii::t('view', 'Tambah Data').' '.Yii::t('view', 'Simak');
$this->breadcrumbs=array(
	Yii::t('view', 'Simak') => array($this->defaultAction),
	Yii::t('view', 'Buat Data Baru'),
);
?>
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <?php echo $this->renderPartial('_form', array('model'=>$model,'data'=>$data)); ?>        </div>
    </div>
</div>
