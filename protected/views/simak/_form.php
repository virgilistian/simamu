<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>Yii::app()->controller->id.'-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form',
    ),
)); ?>
<?php echo $form->errorSummary($model); ?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Data Simak</h3>
  </div>
  <div class="panel-body">
        <p class="help-block">Field dengan tanda <span class="required">*</span> tidak boleh kosong.</p>
                                <?php echo $form->select2Group($model,'tahapan_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['tahapan'],'htmlOptions'=>array('empty' => Yii::t('app', 'Pilih Tahapan...'),'class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                                <?php echo $form->select2Group($model,'instruksi_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['instruksi'],'htmlOptions'=>array('empty' => Yii::t('app', 'Pilih Instruksi...'),'class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                                <?php echo $form->select2Group($model,'proses_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['proses'],'htmlOptions'=>array('empty' => Yii::t('app', 'Pilih Proses...'),'class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                                <?php echo $form->select2Group($model,'checklist_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['checklist'],'htmlOptions'=>array('empty' => Yii::t('app', 'Pilih Checklist...'),'class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                                <?php echo $form->select2Group($model,'acuan_id',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>$data['acuan'],'htmlOptions'=>array('empty' => Yii::t('app', 'Pilih Acuan...'),'class'=>'form-control form-cascade-control','maxlength'=>2)))); ?>
                     <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => Yii::t('app', 'Simpan'),
                ));
                ?>
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'reset',
                    'context' => 'danger',
                    'label' => Yii::t('app', 'Batal'),
                    'htmlOptions' => array('onclick' => 'history.go(-1);'),
                ));
                ?>
            </div>
        </div>
  </div>
</div>
<?php $this->endWidget(); ?>
<?php (Yii::app()->booster) ? Yii::app()->booster->registerYiiCss() : null; ?>
