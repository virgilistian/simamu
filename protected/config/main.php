<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Sistem Informasi Manajemen Mutu',
        'theme'=>'public',
        'language'=>'id',
        'timezone'=>'Asia/Jakarta',

	// preloading 'log' component
	'preload'=>array('log','booster'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'application.extensions.booster.components.Booster',
                'application.modules.srbac.controllers.SBaseController',
	),

	'aliases' => array(
		 'xupload' => 'ext.xupload'
	),
        'modules' => require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'modules.php'),

	// application components
	'components'=>array(
                'booster' => array(
                    'class' => 'ext.booster.components.Booster',
                    //'jqueryCss' => false,
                    //'enableJS' => true,
                    'bootstrapCss' => false,
                    'enableBootboxJS' => false,
                    'enableNotifierJS' => false,
                    'enableCdn' => false,
                    'tooltipSelector' => '[data-toggle=tooltip]'
                ),
                'authManager'=>array(
                    // The type of Manager (Database)
                    'class' => 'application.modules.srbac.components.SDbAuthManager',
                    // The database connection used
                    'connectionID'=>'db',
                    // The itemTable name (default:authitem)
                    'itemTable'=>'authitem',
                    // The assignmentTable name (default:authassignment)
                    'assignmentTable'=>'authassignment',
                    // The itemChildTable name (default:authitemchild)
                    'itemChildTable'=>'authitemchild',
                 ),
		'user'=>array(
                        'class'=>'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
                        'authTimeout' => 3600,
		),
                'userDetail' => array(
                    'class' => 'application.components.UserDetail',
                ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'urlSuffix' => '/',
                        'showScriptName' => false,
                        'cacheID' => 'cache',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
                // database settings are configured in database.php
                'db' => require(dirname(__FILE__) . '/database.php'),
            
                'image' => array(
                    'class' => 'application.extensions.image.CImageComponent',
                    'driver' => 'GD',
                ),
	
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				
				/* array(
					'class'=>'CWebLogRoute',
					'levels'=>'error, warning, trace',
				), */
				
			),
		),
                'setting' => array(
                    'class' => 'application.components.Setting'
                ),
                'generator' => array(
                    'class' => 'RGenerator',
                ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
                'attachDir' => dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'uploaded_files' . DIRECTORY_SEPARATOR . 'attachment',
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);