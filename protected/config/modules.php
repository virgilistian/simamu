<?php

return array(
    // uncomment the following to enable the Gii tool	
    'gii' => array(
        'class' => 'system.gii.GiiModule',
        'password' => '123',
        'ipFilters' => array('127.0.0.1', '::1'),
        'generatorPaths' => array(
            'ext.booster.gii',
        ),
    ),
  'srbac'=>
    array(
      // Your application's user class (default: User)
      'userclass'=>'User',
      // Your users' table user_id column (default: userid)
      'userid'=>'id',
      // your users' table username column (default: username)
      'username'=>'username',
      'delimeter' => '@',
      // Debug mode(default: false)
      // In debug mode every user (even guest) can admin srbac, also
      //if you use internationalization untranslated words/phrases
      //will be marked with a red star
      'debug'=>false,
      // The number of items shown in each page (default:15)
      'pageSize'=>10,
      // The name of the super user
      'superUser' =>'Authority',
      //The css file to use
      'css'=>'srbac.css', // must be in srbac css folder
      //The layout to use
      'layout'=>'webroot.themes.admin.views.layouts.main',
      //The not authorized page to render when a user tries to access an page
      //tha he's not authorized to
      'notAuthorizedView'=>'srbac.views.authitem.unauthorized',
      // The actions that are always allowed to every user (when using the
      // auto create mode of srbac)
      'alwaysAllowed'=>array(
        'Site.','SiteLogout',
        'SiteError', 'SiteContact'),
      // The operationa assigned to users (when using the
      // auto create mode of srbac)
//      'userActions'=>array(
//        'Show','View','List'
//      ),
      //The number of lines in assign listboxes (default 10)
      'listBoxNumberOfLines' => 15,
        'imagesPath' => 'srbac.images',
        'imagesPack' => 'tango',
        'iconText' => true,
  )
);
