<?php

/**
 * This is the model class for table "t_simak".
 *
 * The followings are the available columns in table 't_simak':
 * @property string $id
 * @property string $tahapan_id
 * @property string $instruksi_id
 * @property string $proses_id
 * @property string $checklist_id
 * @property string $acuan_id
 * @property string $created
 * @property string $modified
 * @property string $created_by
 * @property string $modified_by
 *
 * The followings are the available model relations:
 * @property User $modifiedBy
 * @property MAcuan $acuan
 * @property MChecklist $checklist
 * @property MInstruksi $instruksi
 * @property MProses $proses
 * @property MTahapan $tahapan
 * @property User $createdBy
 */
class Simak extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_simak';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tahapan_id, instruksi_id, proses_id, checklist_id', 'required', 'on'=>'create'),
			array('tahapan_id, instruksi_id, proses_id, checklist_id, acuan_id, created_by, modified_by', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tahapan_id, instruksi_id, proses_id, checklist_id, acuan_id, created, modified, created_by, modified_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
			'acuan' => array(self::BELONGS_TO, 'Acuan', 'acuan_id'),
			'checklist' => array(self::BELONGS_TO, 'Checklist', 'checklist_id'),
			'instruksi' => array(self::BELONGS_TO, 'Instruksi', 'instruksi_id'),
			'proses' => array(self::BELONGS_TO, 'Proses', 'proses_id'),
			'tahapan' => array(self::BELONGS_TO, 'Tahapan', 'tahapan_id'),
			'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tahapan_id' => 'Tahapan',
			'instruksi_id' => 'Instruksi',
			'proses_id' => 'Proses',
			'checklist_id' => 'Checklist',
			'acuan_id' => 'Acuan',
			'created' => 'Created',
			'modified' => 'Perubahan Terakhir',
			'created_by' => 'Created By',
			'modified_by' => 'Modified By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tahapan_id',$this->tahapan_id,true);
		$criteria->compare('instruksi_id',$this->instruksi_id,true);
		$criteria->compare('proses_id',$this->proses_id,true);
		$criteria->compare('checklist_id',$this->checklist_id,true);
		$criteria->compare('acuan_id',$this->acuan_id,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('modified_by',$this->modified_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Simak the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function beforeSave()
        {
            if($this->isNewRecord)
            {
                $this->created = new CDbExpression('NOW()');
                $this->modified = new CDbExpression('NOW()');
                $this->created_by = Yii::app()->user->id;
                $this->modified_by = Yii::app()->user->id;
            } else {
                $this->modified = new CDbExpression('NOW()');
                $this->modified_by = Yii::app()->user->id;
            }
            return parent::beforeSave();
        }
}
