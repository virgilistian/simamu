<?php

/**
 * This is the model class for table "t_simak_checklist".
 *
 * The followings are the available columns in table 't_simak_checklist':
 * @property string $id
 * @property string $jembatan_id
 * @property string $tahapan_id
 * @property string $instruksi_id
 * @property string $proses_id
 * @property string $checklist_id
 * @property integer $status
 * @property string $keterangan
 * @property string $created
 * @property string $modified
 * @property string $created_by
 * @property string $modified_by
 *
 * The followings are the available model relations:
 * @property Checklist $checklist
 * @property Instruksi $instruksi
 * @property Jembatan $jembatan
 * @property Proses $proses
 * @property Tahapan $tahapan
 * @property Proyek $acuan
 */
class SimakChecklist extends CActiveRecordC
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_simak_checklist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jembatan_id, tahapan_id, instruksi_id, proses_id, checklist_id', 'required', 'on'=>'create'),
			array('tahapan_id', 'required', 'on'=>'export'),
			array('jembatan_id, tahapan_id, instruksi_id, proses_id', 'required'),
                        array('keterangan', 'required', 'on'=>'addKeterangan'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('jembatan_id, tahapan_id, instruksi_id, proses_id, checklist_id', 'length', 'max'=>11),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, acuan_id, jembatan_id, tahapan_id, instruksi_id, proses_id, checklist_id, status, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'checklist' => array(self::BELONGS_TO, 'Checklist', 'checklist_id'),
			'instruksi' => array(self::BELONGS_TO, 'Instruksi', 'instruksi_id'),
			'jembatan' => array(self::BELONGS_TO, 'Jembatan', 'jembatan_id'),
			'proses' => array(self::BELONGS_TO, 'Proses', 'proses_id'),
			'tahapan' => array(self::BELONGS_TO, 'Tahapan', 'tahapan_id'),
			'acuan' => array(self::BELONGS_TO, 'Acuan', 'acuan_id'),
			'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
			'attachment' => array(self::HAS_MANY, 'Attachment', 'simak_checklist_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jembatan_id' => 'Jembatan',
			'tahapan_id' => 'Tahapan',
			'instruksi_id' => 'Instruksi',
			'proses_id' => 'Proses',
			'checklist_id' => 'Checklist',
			'status' => 'Status',
			'keterangan' => 'Keterangan',
			'acuan_id' => 'Acuan',
			'modified' => 'Perubahan Terakhir',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('jembatan_id',$this->jembatan_id,true);
		$criteria->compare('tahapan_id',$this->tahapan_id,true);
		$criteria->compare('instruksi_id',$this->instruksi_id,true);
		$criteria->compare('proses_id',$this->proses_id,true);
		$criteria->compare('checklist_id',$this->checklist_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('acuan_id',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SimakChecklist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function beforeSave()
        {
            if($this->isNewRecord)
            {
                $this->created = new CDbExpression('NOW()');
                $this->modified = new CDbExpression('NOW()');
                $this->created_by = Yii::app()->user->id;
                $this->modified_by = Yii::app()->user->id;
            } else {
                $this->modified = new CDbExpression('NOW()');
                $this->modified_by = Yii::app()->user->id;
            }
            return parent::beforeSave();
        }
}
