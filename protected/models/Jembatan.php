<?php

/**
 * This is the model class for table "m_jembatan".
 *
 * The followings are the available columns in table 'm_jembatan':
 * @property string $id
 * @property string $balai_id
 * @property string $no
 * @property string $nama
 * @property string $asal
 * @property string $tujuan
 * @property double $km
 * @property string $tipe
 * @property integer $jmlbentang
 * @property double $panjang
 * @property double $lebar
 * @property string $thbuat
 * @property string $is_active
 * @property string $created
 * @property string $modified
 * @property string $created_by
 * @property string $modified_by
 * @property string $last_export_smm
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Jembatan extends CActiveRecord {

    public $jumlah;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'm_jembatan';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('no, nama, balai_id', 'required', 'on' => 'create'),
            array('jmlbentang, balai_id', 'numerical', 'integerOnly' => true),
            array('km, panjang, lebar', 'numerical'),
            array('no', 'length', 'max' => 100),
            array('nama, asal, tujuan, tipe', 'length', 'max' => 255),
            array('is_active', 'length', 'max' => 1),
            array('thbuat', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, balai_id, jumlah, no, nama, asal, tujuan, km, tipe, jmlbentang, panjang, lebar, thbuat, last_export_smm', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'simakChecklist' => [self::HAS_MANY, 'SimakChecklist', 'jembatan_id'],
            'modifiedBy' => [self::BELONGS_TO, 'User', 'modified_by'],
            'balai' => [self::BELONGS_TO, 'Balai', 'balai_id'],
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'balai_id' => 'Balai',
            'no' => 'No',
            'nama' => 'Nama',
            'asal' => 'Asal',
            'tujuan' => 'Tujuan',
            'km' => 'KM',
            'tipe' => 'Tipe',
            'jmlbentang' => 'Jumlah Bentang',
            'panjang' => 'Panjang',
            'lebar' => 'Lebar',
            'thbuat' => 'Tahun Buat',
            'is_active' => 'Status Aktif',
            'modified' => 'Perubahan Terakhir',
            'last_export_smm' => 'Ekspor Data SMM Terakhir',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('balai_id', $this->balai_id, true);
        $criteria->compare('no', $this->no, true);
        $criteria->compare('nama', $this->nama, true);
        $criteria->compare('asal', $this->asal, true);
        $criteria->compare('tujuan', $this->tujuan, true);
        $criteria->compare('km', $this->km);
        $criteria->compare('tipe', $this->tipe, true);
        $criteria->compare('jmlbentang', $this->jmlbentang);
        $criteria->compare('panjang', $this->panjang);
        $criteria->compare('lebar', $this->lebar);
        $criteria->compare('thbuat', $this->thbuat, true);
        $criteria->compare('is_active', $this->is_active, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Jembatan the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->created = new CDbExpression('NOW()');
            $this->modified = new CDbExpression('NOW()');
            $this->created_by = Yii::app()->user->id;
            $this->modified_by = Yii::app()->user->id;
        } else {
            $this->modified = new CDbExpression('NOW()');
            $this->modified_by = Yii::app()->user->id;
        }
        return parent::beforeSave();
    }

}
