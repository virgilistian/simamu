<?php

abstract class CActiveRecordC extends CActiveRecord {

    public function getDateDay($date) {
        return strftime("%A, %d %B %Y", strtotime($date));
    }

    public function getTime($date) {
        return strftime("%H:%M", strtotime($date));
    }

    public function getDate($date) {
        return strftime("%d/%m/%Y", strtotime($date));
    }

    public function getDateTime($date) {
        return strftime("%d/%m/%Y %H:%M", strtotime($date));
    }

    public function getDateMonth($date) {
        return strftime("%d %B %Y", strtotime($date));
    }

    public function getBulan($bln = '01') {
        return ($bln == '00') ? '-' : strftime('%B', strtotime($bln . '/1/2013'));
    }

    public function getCurrency($curr, $number, $dec = 2) {
        $locale = localeconv();
        $curr = isset($curr) ? $curr : $locale['currency_symbol'];
        return $curr . '. ' . number_format($number, $dec, $locale['decimal_point'], $locale['thousands_sep']);
    }

    public function getNumber($number, $dec = 2, $satuan = '') {
        $locale = localeconv();
        $satuan = isset($satuan) ? ' ' . $satuan : '';
        return number_format(abs($number), $dec, $locale['decimal_point'], $locale['thousands_sep']) . $satuan;
    }

    public function listBulan() {
        $bulanList = array(
            '' => Yii::t('app', 'Please select...'),
            '01' => strftime('%B', strtotime('1/1/2013')),
            '02' => strftime('%B', strtotime('2/1/2013')),
            '03' => strftime('%B', strtotime('3/1/2013')),
            '04' => strftime('%B', strtotime('4/1/2013')),
            '05' => strftime('%B', strtotime('5/1/2013')),
            '06' => strftime('%B', strtotime('6/1/2013')),
            '07' => strftime('%B', strtotime('7/1/2013')),
            '08' => strftime('%B', strtotime('8/1/2013')),
            '09' => strftime('%B', strtotime('9/1/2013')),
            '10' => strftime('%B', strtotime('10/1/2013')),
            '11' => strftime('%B', strtotime('11/1/2013')),
            '12' => strftime('%B', strtotime('12/1/2013')),
        );
        return $bulanList;
    }

    public function getBulanShort($bln = '01') {
        return strftime('%b', strtotime($bln . '/1/2013'));
    }

    public function listBulanShort() {
        $bulanList = array(
            strftime('%b', strtotime('1/1/2013')),
            strftime('%b', strtotime('2/1/2013')),
            strftime('%b', strtotime('3/1/2013')),
            strftime('%b', strtotime('4/1/2013')),
            strftime('%b', strtotime('5/1/2013')),
            strftime('%b', strtotime('6/1/2013')),
            strftime('%b', strtotime('7/1/2013')),
            strftime('%b', strtotime('8/1/2013')),
            strftime('%b', strtotime('9/1/2013')),
            strftime('%b', strtotime('10/1/2013')),
            strftime('%b', strtotime('11/1/2013')),
            strftime('%b', strtotime('12/1/2013')),
        );
        return $bulanList;
    }

    public function getRange($tableName, $field) {
        $range = Yii::app()->db->createCommand("SELECT MIN($field) as min_, MAX($field) as max_ FROM " . $tableName)->queryRow();
        return $range;
    }

    public function getRangeTahun($tableName, $field = 'Tahun') {
        $range = Yii::app()->db->createCommand("SELECT MIN($field) as min_tahun, MAX($field) as max_tahun FROM " . $tableName)->queryRow();
        return $range;
    }

    public function getDropDownList($tableName, $field = 'Tahun') {
        $range = $this->getRange($tableName, $field);
        $result = array();
        $result[''] = Yii::t('app', 'Please select...');
        if (!empty($range)) {
            for ($i = $range['max_']; $i >= $range['min_']; $i--) {
                $result[$i] = $i;
            }
        }
        return $result;
    }

    public function wp_trim($str, $width = 60, $break = "|") {
        $formatted = '';
        $position = -1;
        $prev_position = 0;
        $last_line = -1;
        while ($position = mb_stripos($str, " ", ++$position, 'utf-8')) {
            if ($position > $last_line + $width + 1) {
                $formatted.= mb_substr($str, $last_line + 1, $prev_position - $last_line - 1, 'utf-8') . $break;
                $last_line = $prev_position;
            }
            $prev_position = $position;
        }
        $formatted.= mb_substr($str, $last_line + 1, mb_strlen($str), 'utf-8');
        $words = explode('|', $formatted);
        return (count($words) > 1) ? trim($words[0]) . " ..." : trim($words[0]);
    }

    public function class2id($name) {
        return trim(strtolower(str_replace('_', '-', preg_replace('/(?<![A-Z])[A-Z]/', '-\0', $name))), '-');
    }

    protected function excel_to_array($filename, $startRow = 1, $endRow = null, $columns = array(), $headerRow = null) {
        $r = new YiiReport(array('templatePath' => 'masterPath', 'template' => $filename));
        $array = $r->read($startRow, $endRow, $columns);
        if (isset($headerRow) && !empty($headerRow)) {
            $header = $r->read($headerRow, $headerRow, $columns);
            $array = $header + $array;
        }
        return $array;
    }

    protected function sluggish($teks) {
        $text = (strlen($teks) >= 226) ? substr($teks, 0, 225) : $teks;
        return trim(strtolower(preg_replace('/([^\pL\pN])+/u', '-', trim(strtr(str_replace('\'', '', $text), array(
            'Ǎ' => 'A', 'А' => 'A', 'Ā' => 'A', 'Ă' => 'A', 'Ą' => 'A', 'Å' => 'A',
            'Ǻ' => 'A', 'Ä' => 'Ae', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A', 'Â' => 'A',
            'Æ' => 'AE', 'Ǽ' => 'AE', 'Б' => 'B', 'Ç' => 'C', 'Ć' => 'C', 'Ĉ' => 'C',
            'Č' => 'C', 'Ċ' => 'C', 'Ц' => 'C', 'Ч' => 'Ch', 'Ð' => 'Dj', 'Đ' => 'Dj',
            'Ď' => 'Dj', 'Д' => 'Dj', 'É' => 'E', 'Ę' => 'E', 'Ё' => 'E', 'Ė' => 'E',
            'Ê' => 'E', 'Ě' => 'E', 'Ē' => 'E', 'È' => 'E', 'Е' => 'E', 'Э' => 'E',
            'Ë' => 'E', 'Ĕ' => 'E', 'Ф' => 'F', 'Г' => 'G', 'Ģ' => 'G', 'Ġ' => 'G',
            'Ĝ' => 'G', 'Ğ' => 'G', 'Х' => 'H', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ï' => 'I',
            'Ĭ' => 'I', 'İ' => 'I', 'Į' => 'I', 'Ī' => 'I', 'Í' => 'I', 'Ì' => 'I',
            'И' => 'I', 'Ǐ' => 'I', 'Ĩ' => 'I', 'Î' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J',
            'Й' => 'J', 'Я' => 'Ja', 'Ю' => 'Ju', 'К' => 'K', 'Ķ' => 'K', 'Ĺ' => 'L',
            'Л' => 'L', 'Ł' => 'L', 'Ŀ' => 'L', 'Ļ' => 'L', 'Ľ' => 'L', 'М' => 'M',
            'Н' => 'N', 'Ń' => 'N', 'Ñ' => 'N', 'Ņ' => 'N', 'Ň' => 'N', 'Ō' => 'O',
            'О' => 'O', 'Ǿ' => 'O', 'Ǒ' => 'O', 'Ơ' => 'O', 'Ŏ' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ö' => 'Oe', 'Õ' => 'O', 'Ó' => 'O', 'Ò' => 'O', 'Ô' => 'O',
            'Œ' => 'OE', 'П' => 'P', 'Ŗ' => 'R', 'Р' => 'R', 'Ř' => 'R', 'Ŕ' => 'R',
            'Ŝ' => 'S', 'Ş' => 'S', 'Š' => 'S', 'Ș' => 'S', 'Ś' => 'S', 'С' => 'S',
            'Ш' => 'Sh', 'Щ' => 'Shch', 'Ť' => 'T', 'Ŧ' => 'T', 'Ţ' => 'T', 'Ț' => 'T',
            'Т' => 'T', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ū' => 'U', 'Ǜ' => 'U', 'Ǚ' => 'U', 'Ù' => 'U', 'Ú' => 'U', 'Ü' => 'Ue',
            'Ǘ' => 'U', 'Ǖ' => 'U', 'У' => 'U', 'Ư' => 'U', 'Ǔ' => 'U', 'Û' => 'U',
            'В' => 'V', 'Ŵ' => 'W', 'Ы' => 'Y', 'Ŷ' => 'Y', 'Ý' => 'Y', 'Ÿ' => 'Y',
            'Ź' => 'Z', 'З' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z', 'Ж' => 'Zh', 'á' => 'a',
            'ă' => 'a', 'â' => 'a', 'à' => 'a', 'ā' => 'a', 'ǻ' => 'a', 'å' => 'a',
            'ä' => 'ae', 'ą' => 'a', 'ǎ' => 'a', 'ã' => 'a', 'а' => 'a', 'ª' => 'a',
            'æ' => 'ae', 'ǽ' => 'ae', 'б' => 'b', 'č' => 'c', 'ç' => 'c', 'ц' => 'c',
            'ċ' => 'c', 'ĉ' => 'c', 'ć' => 'c', 'ч' => 'ch', 'ð' => 'dj', 'ď' => 'dj',
            'д' => 'dj', 'đ' => 'dj', 'э' => 'e', 'é' => 'e', 'ё' => 'e', 'ë' => 'e',
            'ê' => 'e', 'е' => 'e', 'ĕ' => 'e', 'è' => 'e', 'ę' => 'e', 'ě' => 'e',
            'ė' => 'e', 'ē' => 'e', 'ƒ' => 'f', 'ф' => 'f', 'ġ' => 'g', 'ĝ' => 'g',
            'ğ' => 'g', 'г' => 'g', 'ģ' => 'g', 'х' => 'h', 'ĥ' => 'h', 'ħ' => 'h',
            'ǐ' => 'i', 'ĭ' => 'i', 'и' => 'i', 'ī' => 'i', 'ĩ' => 'i', 'į' => 'i',
            'ı' => 'i', 'ì' => 'i', 'î' => 'i', 'í' => 'i', 'ï' => 'i', 'ĳ' => 'ij',
            'ĵ' => 'j', 'й' => 'j', 'я' => 'ja', 'ю' => 'ju', 'ķ' => 'k', 'к' => 'k',
            'ľ' => 'l', 'ł' => 'l', 'ŀ' => 'l', 'ĺ' => 'l', 'ļ' => 'l', 'л' => 'l',
            'м' => 'm', 'ņ' => 'n', 'ñ' => 'n', 'ń' => 'n', 'н' => 'n', 'ň' => 'n',
            'ŉ' => 'n', 'ó' => 'o', 'ò' => 'o', 'ǒ' => 'o', 'ő' => 'o', 'о' => 'o',
            'ō' => 'o', 'º' => 'o', 'ơ' => 'o', 'ŏ' => 'o', 'ô' => 'o', 'ö' => 'oe',
            'õ' => 'o', 'ø' => 'o', 'ǿ' => 'o', 'œ' => 'oe', 'п' => 'p', 'р' => 'r',
            'ř' => 'r', 'ŕ' => 'r', 'ŗ' => 'r', 'ſ' => 's', 'ŝ' => 's', 'ș' => 's',
            'š' => 's', 'ś' => 's', 'с' => 's', 'ş' => 's', 'ш' => 'sh', 'щ' => 'shch',
            'ß' => 'ss', 'ţ' => 't', 'т' => 't', 'ŧ' => 't', 'ť' => 't', 'ț' => 't',
            'у' => 'u', 'ǘ' => 'u', 'ŭ' => 'u', 'û' => 'u', 'ú' => 'u', 'ų' => 'u',
            'ù' => 'u', 'ű' => 'u', 'ů' => 'u', 'ư' => 'u', 'ū' => 'u', 'ǚ' => 'u',
            'ǜ' => 'u', 'ǔ' => 'u', 'ǖ' => 'u', 'ũ' => 'u', 'ü' => 'ue', 'в' => 'v',
            'ŵ' => 'w', 'ы' => 'y', 'ÿ' => 'y', 'ý' => 'y', 'ŷ' => 'y', 'ź' => 'z',
            'ž' => 'z', 'з' => 'z', 'ż' => 'z', 'ж' => 'zh'
                ))))), '-');
    }

    public function getStatusPermohonan($id, $field = 'nama') {
        $permohonan = StatusPermohonan::model()->findByPk($id);
        $role = Yii::app()->userDetail->getRoleName();
        if ($permohonan && !in_array($role, array('member', 'guest', 'perusahaan'))) {
            return $permohonan->$field;
        } else {
            return Yii::t('app', 'Sedang diproses');
        }
    }

}
