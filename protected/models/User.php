<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $balai_id
 * @property string $jembatan
 * @property string $username
 * @property string $fullname
 * @property string $password
 * @property string $ssid
 * @property string $created
 * @property string $modified
 * @property integer $is_active
 * @property integer $is_login
 * @property string $last_visit
 *
 * The followings are the available model relations:
 * @property Jembatan $jembatan
 */
class User extends CActiveRecord {

    public $password_repeat;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, fullname, password, password_repeat', 'required', 'on' => 'insert'),
            array('password_repeat', 'compare', 'compareAttribute' => 'password'),
            array('username', 'unique'),
            array('is_active, is_login', 'numerical', 'integerOnly' => true),
            array('balai_id', 'length', 'max' => 11),
            array('username, fullname, password', 'length', 'max' => 100),
            array('jembatan', 'length', 'max' => 255),
            array('password_repeat, password, ssid, jembatan', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, balai_id, username, fullname, password, created, modified, is_active, is_login, last_visit, ssid, jembatan', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'modifiedBy' => array(self::HAS_MANY, 'Jembatan', 'modified_by'),
            'modifiedBy' => array(self::HAS_MANY, 'SimakChecklist', 'modified_by'),
            'balai' => array(self::BELONGS_TO, 'Balai', 'balai_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'balai_id' => 'User Balai',
            'jembatan' => 'User Jembatan',
            'fullname' => 'Nama Lengkap',
            'password' => 'Password',
            'created' => 'Created',
            'modified' => 'Perubahan Terakhir',
            'is_active' => 'Status Aktif',
            'is_login' => 'Login',
            'last_visit' => 'Login Terakhir',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('balai_id', $this->balai_id, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('fullname', $this->fullname, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('modified', $this->modified, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('is_login', $this->is_login);
        $criteria->compare('last_visit', $this->last_visit, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->created = new CDbExpression('NOW()');
            $this->modified = new CDbExpression('NOW()');
            $this->password = $this->hashPassword($this->password);
        } else {
            $this->modified = new CDbExpression('NOW()');
        }
        return parent::beforeSave();
    }

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @return boolean whether the password is valid
     */
    public function validatePassword($password) {
        return CPasswordHelper::verifyPassword($password, $this->password);
    }

    /**
     * Generates the password hash.
     * @param string password
     * @return string hash
     */
    public static function hashPassword($password) {
        return CPasswordHelper::hashPassword($password);
    }

}
