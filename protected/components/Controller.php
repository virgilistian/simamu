<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function init() {
        parent::init();
        $locale = (Yii::app()->language == 'en' || Yii::app()->language == 'en_us') ? $this->getLocaleOS("en") : $this->getLocaleOS();
        setlocale(LC_ALL, $locale);
        // Check only when the user is logged in
    }

    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        // Check only when the user is logged in
        if (!Yii::app()->user->isGuest) {
            $user = User::model()->findByPk(Yii::app()->user->id);
            $ssid = session_id();
            if ((Yii::app()->user->getState('userSessionTimeout') < time()) || ($user->ssid !== $ssid)) {
                Yii::app()->user->logout();
                $this->redirect(Yii::app()->homeUrl);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    protected function getLocaleOS($loc = "id") {
        $os = (php_uname()) ? php_uname() : PHP_OS;
        if ($loc == "en") {
            $locale = (strtoupper(substr($os, 0, 3)) === 'WIN') ? "American" : "en_US";
        } else {
            $locale = (strtoupper(substr($os, 0, 3)) === 'WIN') ? "Indonesian" : "id_ID";
        }
        return $locale;
    }

    public function filterAdminTheme($c) {
        $action = array('login');
        if (!Yii::app()->user->isGuest) {
            if (in_array(Yii::app()->controller->action->id, $action)) {
                Yii::app()->theme = 'public';
            } else {
                Yii::app()->theme = 'admin';
            }
        }
        $c->run();
    }

    public function filters() {
        return array(
            'accessControl',
            'adminTheme',
            array('application.components.AuthFilter')
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('login'),
                'users' => array('*'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function accessDenied($message = null) {
        if ($message === null) {
            $message = Yii::t('app', 'Anda tidak berhak mengakses halaman yang dituju.');
        }
        $user = Yii::app()->getUser();
        if ($user->isGuest === true) {
            $user->loginRequired();
        } else {
            Yii::app()->user->setFlash('error', $message);
            $this->redirect(array('/'));
        }
    }

    protected function getNumber($number, $dec = 2, $satuan = '') {
        $locale = localeconv();
        $satuan = isset($satuan) ? ' ' . $satuan : '';
        return number_format(abs($number), $dec, $locale['decimal_point'], $locale['thousands_sep']) . $satuan;
    }

    public function getDateMonth($date) {
        return strftime("%d %B %Y", strtotime($date));
    }

    public function getDateTime($date) {
        return strftime("%d/%m/%Y %H:%M", strtotime($date));
    }

    public function getNamaBalai($id) {
        $data = Balai::model()->findByPk($id);
        if ($data)
            return $data->nama;
        return false;
    }

    public function getNamaJembatan($id) {
        $data = Jembatan::model()->findByPk($id);
        if ($data)
            return $data->nama;
        return false;
    }

    public function getTahapan($id) {
        $data = Tahapan::model()->findByPk($id);
        if ($data)
            return $data->uraian;
        return false;
    }
    
    public function getLastExportSmm($id) {
        $data = Jembatan::model()->findByPk($id);
        if ($data)
            return $data->last_export_smm;
        return false;
    }

}
