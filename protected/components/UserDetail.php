<?php

/**
 * Description of UserDetail
 *
 * @author Lenovo
 */
class UserDetail extends CApplicationComponent {

    protected $_user = null;

    public function UserDetail() {
        if (!Yii::app()->user->isGuest) {
            $out = array();
            $userId = Yii::app()->user->id;
            $user = User::model()->findByPk($userId);
            if ($user) {
                $out = $user->attributes;
            }
            $auths = Yii::app()->authManager->getAuthItems(null, $userId);
            if ($auths) {
                $out['auth'] = array_keys($auths);
            }
            $this->setUser($out);
        }
    }

    public function getUser() {
        return $this->_user;
    }

    public function setUser($user) {
        $this->_user = $user;
    }

    public function getDisplayName() {
        return !empty($this->_user['fullname']) ? $this->_user['fullname'] : (isset($this->_user['username']) ? $this->_user['username'] : 'Guest');
    }
    
    public function getMemberSince() {
        return !empty($this->_user['created']) ? strftime("%B %Y", strtotime($this->_user['created'])) : time();
    }
    
    public function getUserBalai() {
        return $this->_user['balai_id'];
    }

    public function getUserJembatan() {
        return $this->_user['jembatan'];
    }

    public function getAuthorization() {
        return !empty($this->_user['auth']) ? $this->_user['auth'] : null;
    }
    
    public function getRoleName() {
        if (!Yii::app()->user->isGuest) {
            if ($this->getIsAdminRole()) {
                return 'Admin';
            } elseif($this->getIsBalaiRole()) {
                return 'Balai';
            } elseif($this->getIsPpkRole()) {
                return 'PPK';
            } elseif($this->getIsDirektoratRole()) {
                return 'Direktorat';
            } else {
                return 'Member';
            }
        }
    }

    public function getIsAdminRole() {
        if (isset($this->_user['auth'])) {
            if (in_array('Admin', $this->_user['auth']) !== false) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function getIsPpkRole() {
        if (isset($this->_user['auth'])) {
            if (in_array('PPK', $this->_user['auth']) !== false) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function getIsBalaiRole() {
        if (isset($this->_user['auth'])) {
            if (in_array('Balai', $this->_user['auth']) !== false) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function getIsDirektoratRole() {
        if (isset($this->_user['auth'])) {
            if (in_array('Direktorat', $this->_user['auth']) !== false) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    
    public function getAvatar() {
        return isset($this->_user['avatar']) ? strip_tags($this->_user['avatar']) : '/files/no-avatar.png';
    }

    public function getField($field) {
        return isset($this->_user[$field]) ? $this->_user[$field] : null;
    }

    public function hasAuthorityRole() {
        if (isset($this->_user['auth'])) {
            if (in_array('Authority', $this->_user['auth']) !== false) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}

?>