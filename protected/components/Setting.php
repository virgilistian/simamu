<?php

class Setting extends CApplicationComponent
{
    public function addFiles($id) {
        if( Yii::app( )->user->hasState( 'files' ) ) {
            $userFiles = Yii::app()->user->getState( 'files' );
            $path = Yii::app( )->getBasePath( )."/../files/";
            foreach( $userFiles as $file ) {
                if( is_file( $file["path"] ) ) {
                    if( rename( $file["path"], $path.$file["filename"] ) ) {
                        $att = new Attachment;
                        $att->size = $file["size"];
                        $att->type = $file["mime"];
                        $att->name = $file["name"];
                        $att->filename = $file["filename"];
                        $att->path = "/files/";
                        $att->simak_checklist_id = $id;
                        
                        if( !$att->save( ) ) {
                            Yii::log( "Could not save File:\n".CVarDumper::dumpAsString( 
                                $att->getErrors( ) ), CLogger::LEVEL_ERROR );
                            throw new Exception( 'Could not save File');
                        }
                    }
                } else {
                    Yii::log( $file["path"]." is not a file", CLogger::LEVEL_WARNING );
                }
            }
            Yii::app( )->user->setState( 'files', null );
        }
    }
    
    public function deleteFiles($id) {
        if(isset($id) && !empty($id)){
            $attachments = Attachment::model()->findAllByAttributes(array('simak_checklist_id'=>$id));
            if(!empty($attachments)){
                foreach ($attachments as $attachment) {
                    $rootDir = YiiBase::getPathOfAlias('webroot').$attachment->path;
                    if(file_exists($rootDir.DIRECTORY_SEPARATOR.$attachment->filename))
                    {
                        unlink($rootDir.DIRECTORY_SEPARATOR.$attachment->filename);
                    }
                }
            }
        }
    }
    
    public function deleteFilesTemp() {
        if( Yii::app( )->user->hasState( 'files' ) ) {
            $userFiles = Yii::app()->user->getState('files');
            foreach ($userFiles as $file) {
                if(file_exists($file['path'])){
                    unlink($file["path"]);
                }
            }
        }
    }
    
    public function class2name($name) {
        return ucwords(trim(strtolower(str_replace(array('-','_'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $name)))));
    }
    
    public function limitChar($content, $limit) {
        if (strlen($content) <= $limit) {
            return $content;
        } else {
            $hasil = substr($content, 0, $limit);
            return $hasil . "...";
        }
    }
}

