<?php

/**
 * Filter that automatically checks if the user has access to the current controller action.
 */
class AuthFilter extends CFilter {

    public $params = array();
    public $_allowedActions = 'Site.*';

    protected function preFilter($filterChain) {
        $allow = true;
        $user = Yii::app()->getUser();
        $controller = $filterChain->controller;
        $action = $filterChain->action;
        if ($this->_allowedActions !== '*' && $controller->id !== 'site') {
            $authItem = '';
            if (($module = $controller->getModule()) !== null) {
                $authItem .= ucfirst($module->id) . '.';
            }
            $authItem .= ucfirst($controller->id);
            if ($user->checkAccess($authItem . '.*') !== true) {
                $authItem .= '.' . ucfirst($action->id);
                if ($user->checkAccess($authItem) !== true) {
                    $allow = false;
                }
            }
        }
        if ($allow === false) {
            $controller->accessDenied();
            return false;
        }
        return true;
    }

}
