<?php

/**
 * ## BootstrapCode class file.
 *
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
Yii::import('gii.generators.crud.CrudCode');

/**
 * ## Class BootstrapCode
 *
 * @package booster.gii
 */
class BootstrapCode extends CrudCode {

    public function generateActiveGroup($modelClass, $column) {

        if ($column->type === 'boolean') {
            return "\$form->checkBoxGroup(\$model,'{$column->name}')";
        } else if (stripos($column->dbType, 'text') !== false) {
            return "\$form->textAreaGroup(\$model,'{$column->name}', array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','rows'=>4, 'cols'=>50))))";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $inputField = 'passwordFieldGroup';
            } else {
                $inputField = 'textFieldGroup';
            }

            if ($column->type !== 'string' || $column->size === null) {
                if ($column->dbType == 'date') {
                    return "\$form->datePickerGroup(\$model,'{$column->name}',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('options'=>array(),'htmlOptions'=>array()), 'prepend'=>'<i class=\"glyphicon glyphicon-calendar\"></i>', 'append'=>'Click on Month/Year to select a different Month/Year.'))";
                } else {
                    return "\$form->{$inputField}(\$model,'{$column->name}',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control'))))";
                }
            } else {
                if (strpos($column->dbType, 'enum(') !== false) {
                    $temp = $column->dbType;
                    $temp = str_replace('enum', 'array', $temp);
                    // FIXME: What. The. Seriously, parse the enum declaration from MySQL as an array definition in PHP?!
                    eval('$options = ' . $temp . ';');
                    $dropdown_options = "array(";
                    foreach ($options as $option) {
                        $dropdown_options .= "\"$option\"=>\"$option\",";
                    }
                    $dropdown_options .= ")";
                    return "\$form->dropDownListGroup(\$model,'{$column->name}', array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('data'=>{$dropdown_options}, 'htmlOptions'=>array('class'=>'form-control'))))";
                } else {
                    return "\$form->{$inputField}(\$model,'{$column->name}',array('wrapperHtmlOptions'=>array('class'=>'col-md-8'),'labelOptions'=>array('class'=>'col-md-2'),'widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control form-cascade-control','maxlength'=>$column->size))))";
                }
            }
        }
    }

    public function guessNameColumn($columns) {
        foreach ($columns as $column) {
            $name_ = explode('_', $column->name);
            $name = isset($name_[1]) ? $name_[1] : $column->name;
            if (!strcasecmp($name, 'name'))
                return $column->name;
        }
        foreach ($columns as $column) {
            $name_ = explode('_', $column->name);
            $name = isset($name_[1]) ? $name_[1] : $column->name;
            if (!strcasecmp($name, 'title'))
                return $column->name;
        }
        foreach ($columns as $column) {
            $name_ = explode('_', $column->name);
            $name = isset($name_[1]) ? $name_[1] : $column->name;
            if (!strcasecmp($name, 'nama'))
                return $column->name;
        }
        foreach ($columns as $column) {
            $name_ = explode('_', $column->name);
            $name = isset($name_[1]) ? $name_[1] : $column->name;
            if (!strcasecmp($name, 'judul'))
                return $column->name;
        }
        foreach ($columns as $column) {
            $name_ = explode('_', $column->name);
            $name = isset($name_[1]) ? $name_[1] : $column->name;
            if (!strcasecmp($name, 'tahun'))
                return $column->name;
        }
        foreach ($columns as $column) {
            $name_ = explode('_', $column->name);
            $name = isset($name_[1]) ? $name_[1] : $column->name;
            if (!strcasecmp($name, 'username'))
                return $column->name;
        }
        foreach ($columns as $column) {
            if ($column->isPrimaryKey)
                return $column->name;
        }
        return 'id';
    }

	protected function generateClassName($tableName)
	{
		if(($pos=strpos($tableName,'.'))!==false) 
			$tableName=substr($tableName,$pos+1);
		$className='';
		foreach(explode('_',$tableName) as $name)
		{
			if($name!=='')
				$className.=ucfirst($name);
		}
		return $className;
	}
    
        protected function getRelatedName($tblName) {
            $tbl_related = Yii::app()->db->getSchema()->getTable($tblName, Yii::app()->db->schemaCachingDuration!==0);
            $rel = array($this->generateClassName($tblName), $this->guessNameColumn($tbl_related->columns));
            return $rel;
        }
}
