<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->class2name($this->modelClass);
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn},
);\n";
        echo "?>\n";
?>
<div class="container">
    <div class="col-md-12">
        <div class="media">
            <a class="pull-left" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>\$cat));?>";?>">
                <img class="media-object" src="<?php echo "<?php echo Yii::app()->theme->baseUrl;?>"; ?>/assets/images/<?php echo "<?php echo \$cat.'.png';?>"; ?>" height="100" alt="">
            </a>
            <div class="media-body">
                <h2 class="media-heading">
                    <?php echo "<?php echo \$model->perusahaan->nama_perusahaan;?>"; ?><h4>(-----)</h4>
                </h2>
                <?php echo "<?php if(\$cat=='administrasi'): ?>\n"; ?>
                <div class="col-md-12 row">
                    <div class="btn-group btn-group-sm">
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>\$cat));?>"; ?>">Administrasi</a>
                        <a class="btn btn-default" href="#">Akta&nbsp;<span class="caret"></span></a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/npwp/view', array('id'=>\$model->perusahaan_id));?>";?>">NPWP</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/domisili/view', array('id'=>\$model->perusahaan_id));?>";?>">Domisili</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesanggupank3/view', array('id'=>\$model->perusahaan_id));?>";?>">K3</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesanggupantaatperuundangan/view', array('id'=>\$model->perusahaan_id));?>";?>">Kesanggupan Perundangan</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/persetujuanpemda/view', array('id'=>\$model->perusahaan_id));?>";?>">Izin Prinsip</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesediaandiinspeksi/view', array('id'=>\$model->perusahaan_id));?>";?>">Kesanggupan Inspeksi</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesanggupanpenugasan/view', array('id'=>\$model->perusahaan_id));?>";?>">Kesanggupan Penugasan</a>
                    </div>
                </div>
                <?php echo "<?php endif;?>\n";?>
            </div>
            </div>
        <div class="col-md-13 view-list">     
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Tanda Daftar Perusahaan</h3>
              </div>
              <div class="panel-body">
                <?php echo "<?php\n 
                \$this->widget('booster.widgets.TbDetailView',array(
                    'data'=>\$model,
                    'type' => 'condensed',
                    'attributes'=>\$this->getViewList(\$model),
                )); ?>\n";?>
                <div class="btn-group col-md-4 col-md-offset-3">
                    <a class="btn btn-primary" href="<?php echo "<?php echo Yii::app()->createUrl('/".strtolower($this->modelClass)."/update', array('id'=>\$model->id));?>";?>">Edit</a>
                </div>
              </div>
            </div>
        </div>
        <div class="other-kategori">
            <div class="form-inline pull-right">
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'teknis'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/teknis.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'perijinan'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/perijinan.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'surat-terkait'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/surat-terkait.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'evaluasi'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/evaluasi.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'pelaporan'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/pelaporan.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'lain-lain'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/lain-lain.png">
                </a>
            </div>
        </div>
    </div>
</div>
</div>