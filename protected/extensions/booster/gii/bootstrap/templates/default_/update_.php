<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Update',
);\n";
echo "?>\n"
?>
<div class="container">
    <div class="col-md-12">
        <div class="media">
            <a class="pull-left" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>\$cat));?>";?>">
                <img class="media-object" src="<?php echo "<?php echo Yii::app()->theme->baseUrl;?>"; ?>/assets/images/<?php echo "<?php echo \$cat.'.png';?>"; ?>" height="100" alt="">
            </a>
            <div class="media-body">
                <h2 class="media-heading">
                    <?php echo "<?php echo \$model->perusahaan->nama_perusahaan;?>"; ?><h4>(-----)</h4>
                </h2>
                <?php echo "<?php if(\$cat=='administrasi'): ?>"; ?>
                <div class="col-md-12 row">
                    <div class="btn-group btn-group-sm">
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>\$cat));?>"; ?>">Administrasi</a>
                        <a class="btn btn-default" href="#">Akta&nbsp;<span class="caret"></span></a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/npwp/view', array('id'=>\$model->id));?>";?>">NPWP</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/domisili/view', array('id'=>\$model->id));?>";?>">Domisili</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesanggupank3/view', array('id'=>\$model->id));?>";?>">K3</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesanggupantaatperuundangan/view', array('id'=>\$model->id));?>";?>">Kesanggupan Perundangan</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/persetujuanpemda/view', array('id'=>\$model->id));?>";?>">Izin Prinsip</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesediaandiinspeksi/view', array('id'=>\$model->id));?>";?>">Kesanggupan Inspeksi</a>
                        <a class="btn btn-default" href="<?php echo "<?php echo Yii::app()->createUrl('/kesanggupanpenugasan/view', array('id'=>\$model->id));?>";?>">Kesanggupan Penugasan</a>
                    </div>
                </div>
                <?php echo "<?php endif;?>\n";?>
            </div>
        <div class="col-md-13">
            <?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>\n"; ?>
        </div>
        <div class="other-kategori">
            <div class="form-inline pull-right">
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'teknis'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/teknis.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'perijinan'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/perijinan.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'surat-terkait'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/surat-terkait.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'evaluasi'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/evaluasi.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'pelaporan'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/pelaporan.png">
                </a>
                <a class="img" href="<?php echo "<?php echo Yii::app()->createUrl('/perusahaan/detail', array('id'=>\$model->perusahaan_id, 'cat'=>'lain-lain'))?>";?>">
                    <img class="img-responsive" src="<?php echo "<?php echo Yii::app()->theme->baseUrl; ?>";?>/assets/images/lain-lain.png">
                </a>
            </div>
        </div>
    </div>
</div>
</div>