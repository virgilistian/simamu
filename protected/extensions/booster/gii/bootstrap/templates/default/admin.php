<?php echo "<?php\n"; ?>
<?php $label=$this->class2name($this->modelClass);?>
$this->pageTitle = Yii::t('view', '<?php echo $label; ?>');
$this->breadcrumbs=array(
	Yii::t('view', '<?php echo $label; ?>') => array('admin'),
);
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo "<?php"?> echo Yii::t('view','Data <?php echo $label; ?>');?></h3>
  </div>
    <div class="panel-body"> 
          
        <div class="btn-group" role="group">
            <?php echo "<?php\n"?>
            echo CHtml::link(Yii::t('view', 'Tambah Data'), array('create'), array('class' => 'btn btn-default'));
            echo CHtml::link(Yii::t('view','Form Pencarian'), '#', array('class'=>'btn btn-default search-button'));
            ?>
        </div>
        <div class="search-form" style="display:none">
            <?php echo "<?php"?> $this->renderPartial('_search',array(
                    'model'=>$model,
                   )); ?>
        </div><!-- search-form -->
<?php echo "<?php"; ?> $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'responsiveTable' => true,
    'columns' => array(
            <?php
            $ignores = array('created','modified','created_by','modified_by');
            foreach ($this->tableSchema->columns as $column) {
                $name_ = explode('_', $column->name, 2);
                $name = isset($name_[1]) ? $name_[1] : $column->name;    
                if ($column->isPrimaryKey || in_array($name, $ignores) || in_array($column->name, $ignores)) {
                    continue;
                } else {
                    echo "array(\n";
                    echo "\t\t'name' => '$column->name',\n";
                    if ($column->allowNull) {
                        echo "\t\t'visible' => false,\n";
                    }
                    if ($column->isForeignKey) {
                        $rel = $this->getRelatedname($this->tableSchema->foreignKeys[$column->name][0]);
                        echo "\t\t'value' => '(\$data->{$rel[0]}) ? \$data->{$rel[0]}->{$rel[1]} : \"-\"',\n";
                    } else {
                        echo "\t\t'value' => '(\$data->{$column->name}) ? \$data->{$column->name} : \"-\"',\n";
                    }
                    echo "\t),";
                }
            }
            ?>
            array(
                'htmlOptions' => array('nowrap'=>'nowrap','class'=>'button-column'),
                'class'=>'booster.widgets.TbButtonColumn'
            ),
        )
    )
); 
?>
    </div>
</div>
<?php echo "<?php\n";?> 
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#".Yii::app()->controller->id."-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });
");
?>