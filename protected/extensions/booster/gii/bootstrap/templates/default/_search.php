<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl(\$this->route),
        'type' => 'horizontal',
	'method'=>'get',
)); ?>\n"; ?>

<fieldset>
    <legend><h4><?php echo "<?php"?> echo Yii::t('view', 'Form Pencarian')?></h4></legend>
<?php foreach ($this->tableSchema->columns as $column): ?>
	<?php
	$field = $this->generateInputField($this->modelClass, $column);
	if (strpos($field, 'password') !== false) {
		continue;
	}
	?>
	<?php echo "<?php echo " . $this->generateActiveGroup($this->modelClass, $column) . "; ?>\n"; ?>

<?php endforeach; ?>
       <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
		<?php echo "<?php \$this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>\n"; ?>
            </div>
	</div>

</fieldset>
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>