<?php echo "<?php\n"; ?>
$this->pageTitle = Yii::t('view', '<?php echo $this->modelClass; ?>');
$this->breadcrumbs=array(
	Yii::t('view', '<?php echo $this->modelClass; ?>') => array('admin'),
);
?>
<?php echo "<?php"; ?> $this->widget('booster.widgets.TbGridView',array(
    'id'=>Yii::app()->controller->id.'-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'columns' => array(
            <?php
            $ignores = array('created_at','modified_at','created_by','modified_by');
            foreach ($this->tableSchema->columns as $column) {
                $name_ = explode('_', $column->name, 2);
                $name = isset($name_[1]) ? $name_[1] : $column->name;    
                if ($column->isPrimaryKey || in_array($name, $ignores) || in_array($column->name, $ignores)) {
                    continue;
                } else {
                    echo "array(\n";
                    echo "\t\t'name' => '$column->name',\n";
                    if ($column->allowNull) {
                        echo "\t\t'visible' => false,\n";
                    }
                    if ($column->isForeignKey) {
                        $rel = $this->getRelatedname($this->tableSchema->foreignKeys[$column->name][0]);
                        echo "\t\t'value' => '(\$data->{$rel[0]}) ? \$data->{$rel[0]}->{$rel[1]} : \"-\"',\n";
                    } else {
                        echo "\t\t'value' => '(\$data->{$column->name}) ? \$data->{$column->name} : \"-\"',\n";
                    }
                    echo "\t),";
                }
            }
            ?>   
        )
    )
); 
?>
<div style="display: none;" tabindex="-1" class="modal fade" id="myModal"></div>