<?php echo "<?php \$form=\$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>Yii::app()->controller->id.'-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form',
    ),
)); ?>\n"; 
?>
<?php $label=$this->class2name($this->modelClass);?>
<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Data <?php echo $label;?></h3>
  </div>
  <div class="panel-body">
        <p class="help-block">Field dengan tanda <span class="required">*</span> tidak boleh kosong.</p>
        <?php
        $ignores = array('created','modified','created_by','modified_by');
        foreach ($this->tableSchema->columns as $column) {
            $name_ = explode('_', $column->name, 2);
            $name = isset($name_[1]) ? $name_[1] : $column->name;
            if ($column->isPrimaryKey || in_array($name, $ignores) || in_array($column->name, $ignores)) {
                continue;
            } else {
                ?>
                <?php echo "<?php echo " . $this->generateActiveGroup($this->modelClass, $column) . "; ?>\n"; ?>
        <?php } ?>
        <?php
        }
        ?>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-8">
                <?php echo "<?php\n"; ?>
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => Yii::t('app', 'Simpan'),
                ));
                ?>
                <?php echo "<?php\n"; ?>
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'reset',
                    'context' => 'danger',
                    'label' => Yii::t('app', 'Batal'),
                    'htmlOptions' => array('onclick' => 'history.go(-1);'),
                ));
                ?>
            </div>
        </div>
  </div>
</div>
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
<?php echo "<?php "; ?>(Yii::app()->booster) ? Yii::app()->booster->registerYiiCss() : null; ?>
