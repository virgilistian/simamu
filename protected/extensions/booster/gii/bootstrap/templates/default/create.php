<?php echo "<?php\n"; 
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
?>
<?php $label=$this->class2name($this->modelClass);?>
$this->pageTitle = Yii::t('view', 'Tambah Data').' '.Yii::t('view', '<?php echo $label; ?>');
$this->breadcrumbs=array(
	Yii::t('view', '<?php echo $label; ?>') => array($this->defaultAction),
	Yii::t('view', 'Buat Data Baru'),
);
?>
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
        </div>
    </div>
</div>
