<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="Virgiawan Listianto (@virgilistian) - 2015">
    <title>Sistem Informasi Manajemen Mutu</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!--<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/css/style.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/iCheck/flat/blue.css">
<!--     Morris chart 
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/morris/morris.css">
     jvectormap 
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
     Date Picker 
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/datepicker/datepicker3.css">
     Daterange picker 
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/daterangepicker/daterangepicker-bs3.css">
     bootstrap wysihtml5 - text editor 
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue<?php echo (Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'welcome') ?  ' layout-top-nav' : ' sidebar-mini';?>">
    <div class="wrapper">
      
          <header>
            <div class="row">
                  <div class="col-lg-8">
                      <a href="<?php echo Yii::app()->baseUrl;?>" class="navbar-brand">
                          <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/logo-pu.png">
                      </a>
                      <h4>
                          <strong>Kementerian Pekerjaan Umum dan Perumahan Rakyat</strong><br>
                              Direktorat Jenderal Bina Marga<br>
                              Direktorat Jembatan
                      </h4>
                  </div>
              </div>
          </header>
        
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo Yii::app()->baseUrl;?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>S</b>MM</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">SMM JEMBATAN</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="display:<?php echo (Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'welcome') ?  'none' : '';?>">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
              <?php require_once 'top_nav.php';?>
          </div>
        </nav>
      </header>
      <!--Only For SRBAC-->  
      <?php if(Yii::app()->controller->id === 'authitem'): ?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo Yii::app()->baseUrl.Yii::app()->userDetail->getAvatar(); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo ucwords(Yii::app()->userDetail->getDisplayName());?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <?php require_once 'sidebar.php';?>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Selamat Datang<small> <?php echo ucwords(Yii::app()->userDetail->getDisplayName());?></small></h1>
            <?php if(isset($this->breadcrumbs)):?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                    'tagName'=>'ol',
                    'htmlOptions'=>array('class'=>'breadcrumb'),
                    'homeLink'=> '<li><a href="'.Yii::app()->homeUrl.'"><i class="fa fa-home"></i> Beranda</a></li>',
                    'links'=>$this->breadcrumbs,
                    'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                    'inactiveLinkTemplate'=>'<li class="active">{label}</li>',
                    'separator'=>false
                )); ?>
            <?php endif?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-info"></i> Informasi:</h4>
                        <?php echo Yii::app()->user->getFlash('info'); ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('notice')): ?>
                <div class="alert alert-warning alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                        <?php echo Yii::app()->user->getFlash('notice'); ?>
                    </div>
            <?php endif; ?>
            <?php echo $content;?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php endif; ?>
      
        <?php
        if(Yii::app()->controller->id !== 'authitem')
            echo $content;
        ?>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <?php require_once 'script_style.php';?>
  </body>
</html>
