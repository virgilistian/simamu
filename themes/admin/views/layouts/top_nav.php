<div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="true">
                  <img alt="User Image" class="user-image" src="<?php echo Yii::app()->baseUrl.Yii::app()->userDetail->getAvatar(); ?>">
                  <span class="hidden-xs"><?php echo ucwords(Yii::app()->userDetail->getDisplayName());?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img alt="User Image" class="img-circle" src="<?php echo Yii::app()->baseUrl.Yii::app()->userDetail->getAvatar(); ?>">
                    <p>
                      <?php echo ucwords(Yii::app()->userDetail->getDisplayName()).' - '.Yii::app()->userDetail->getRoleName();?>
                      <small>Terdaftar sejak <?php echo Yii::app()->userDetail->getMemberSince();?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                        <?php echo CHtml::link(Yii::t('view', '<i class="fa fa-user"></i> Profil'), Yii::app()->createAbsoluteUrl('user/'.Yii::app()->user->id), ['class'=>'btn btn-default btn-flat']);?>
                    </div>
                    <div class="pull-right">
                      <?php echo CHtml::link(Yii::t('view', '<i class="fa fa-power-off"></i> Keluar'), Yii::app()->createAbsoluteUrl('site/logout'), ['class'=>'btn btn-default btn-flat']);?>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>