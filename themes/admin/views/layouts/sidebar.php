<?php
$dataRujukan = ['acuan','instruksi','jembatan','proses','checklist','simak','simakSop','balai','simakChecklist'];
$user = ['authitem','user'];
$display = ' style="display:none"';
$execptAction = ['jembatan','export','detail','perencanaan','pembangunan','preservasi'];
$displayDataRujukan = (Yii::app()->userDetail->getIsAdminRole()) ? '' : 'none';
?>
<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li>
        <?php echo CHtml::link('<i class="fa fa-home"></i> <span>Beranda</span>',  Yii::app()->createAbsoluteUrl('/'));?>
    </li>
    <li class="<?php echo (in_array(Yii::app()->controller->id, $dataRujukan) && !in_array(Yii::app()->controller->action->id, $execptAction)) ? 'active ' : '';?>treeview" style="display: <?php echo $displayDataRujukan;?>">
      <a href="#"><i class="fa fa-pie-chart"></i><span>Data Rujukan</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li<?php echo (Yii::app()->controller->id=='balai') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Data Balai',  Yii::app()->createAbsoluteUrl('balai/'));?>
        </li>
        <li<?php echo (Yii::app()->controller->id=='checklist') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Data Checklist',  Yii::app()->createAbsoluteUrl('checklist/'));?>
        </li>
        <li<?php echo (Yii::app()->controller->id=='acuan') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Acuan',  Yii::app()->createAbsoluteUrl('acuan/'));?>
        </li>
        <li<?php echo (Yii::app()->controller->id=='instruksi') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Instruksi',  Yii::app()->createAbsoluteUrl('instruksi/'));?>
        </li>
        <li<?php echo (Yii::app()->controller->id=='jembatan') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Jembatan',  Yii::app()->createAbsoluteUrl('jembatan/'));?>
        </li>
        <li<?php echo (Yii::app()->controller->id=='proses') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Proses',  Yii::app()->createAbsoluteUrl('proses/'));?>
        </li>
        <li<?php echo (Yii::app()->controller->id=='simak') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Simak',  Yii::app()->createAbsoluteUrl('simak/'));?>
        </li>
        <li<?php echo ((Yii::app()->controller->id=='simakChecklist')  && (!in_array(Yii::app()->controller->action->id, $execptAction))) ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Simak Checklist',  Yii::app()->createAbsoluteUrl('simakChecklist/'));?>
        </li>
      </ul>
    </li>
    <?php if(Yii::app()->userDetail->getIsAdminRole()):?>
    <li class="<?php echo (in_array(Yii::app()->controller->id, $user)) ? 'active ' : '';?>treeview">
      <a href="#"><i class="fa fa-pie-chart"></i><span>Manajemen Pengguna</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
          <li<?php echo (Yii::app()->controller->id === 'authitem') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Hak Akses',   Yii::app()->createAbsoluteUrl('srbac/'));?>
        </li>
        <li<?php echo (Yii::app()->controller->id=='user') ? ' class="active"': '';?>>
            <?php echo CHtml::link('<i class="fa fa-circle-o"></i> Pengguna',  Yii::app()->createAbsoluteUrl('user/'));?>
        </li>
      </ul>
    </li>
    <?php endif;?>
</ul>          