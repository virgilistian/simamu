<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="Virgiawan Listianto (@virgilistian) - 2015">
        <title>Sistem Informasi Manajemen Mutu | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/css/style.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page content-bg">
        <div class="wrapper">
            <header>
                <div class="row">
                    <div class="col-lg-8">
                        <a href="<?php echo Yii::app()->baseUrl; ?>" class="navbar-brand">
                            <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/logo-pu.png">
                        </a>
                        <h4>
                            <strong>Kementerian Pekerjaan Umum dan Perumahan Rakyat</strong><br>
                            Direktorat Jenderal Bina Marga<br>
                            Direktorat Jembatan
                        </h4>
                    </div>
                </div>
            </header>
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-info"></i> Informasi:</h4>
                    <?php echo Yii::app()->user->getFlash('info'); ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('notice')): ?>
                <div class="alert alert-warning alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                    <?php echo Yii::app()->user->getFlash('notice'); ?>
                </div>
            <?php endif; ?>
            <?php echo $content; ?>
            <div class="row">
                <div class="col-md-12 image-box">
                    <div class="col-md-3">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-1.png" class="img-responsive">
                    </div>
                    <div class="col-md-3">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-2.png" class="img-responsive">
                    </div>
                    <div class="col-md-3">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-3.png" class="img-responsive">
                    </div>
                    <div class="col-md-3">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/dist/img/box-4.png" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery 2.1.4 -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
